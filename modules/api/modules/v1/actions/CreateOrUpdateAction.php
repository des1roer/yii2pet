<?php

namespace app\modules\api\modules\v1\actions;


use app\models\PersItem;

class CreateOrUpdateAction extends \yii\rest\CreateAction
{
    /**
     * @return \yii\db\ActiveRecord|\yii\db\ActiveRecordInterface
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\web\ServerErrorHttpException
     */
    public function run()
    {
        $post = \Yii::$app->getRequest()->getBodyParams();
        $cnt = $post['count'];
        unset($post['count']);

        if ($model = PersItem::find()->andWhere($post)->one()) {
            $model->count = $cnt;
            $model->save();
        } else {
            parent::run();
        }
    }
}
