<?php

namespace app\modules\api\modules\v1\controllers;

use app\models\Unit;

class UnitController extends RestController
{
    public $modelClass = Unit::class;
}
