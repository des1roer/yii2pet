<?php

namespace app\modules\api\modules\v1\controllers;

use app\models\Map;

class MapController extends RestController
{
    public $modelClass = Map::class;

    public function actionStep()
    {
        $result = false;
        $data = \Yii::$app->request->post();
        if (
            $data['level'] == $data['stepLevel']
            && abs($data['selfNumber'] - $data['stepNumber']) == 1
        ) {
            $result = true;
        }

        if (
            abs($data['level'] - $data['stepLevel']) == 1
            && (
                ($data['isStart'] == 'true' && $data['selfNumber'] == $data['stepNumber'])
                || (
                    $data['isStart'] == 'false'
                    && ($data['level'] % 2) != 0
                    && abs($data['selfNumber'] - $data['stepNumber']) <= 0
                )
                || (
                    $data['isStart'] == 'false'
                    && ($data['level'] % 2) == 0
                    && abs($data['selfNumber'] - $data['stepNumber']) <= 1
                )
            )
        ) {
            $result = true;
        }


        return $result;
    }
}
