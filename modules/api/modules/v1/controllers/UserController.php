<?php

namespace app\modules\api\modules\v1\controllers;

use app\models\User;

/**
 * Class PersController
 *
 * @link http://j96177ni.beget.tech/yii2basic/web/api/user
 */
class UserController extends RestController
{
    public $modelClass = User::class;
}
