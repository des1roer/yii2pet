<?php

namespace app\modules\api\modules\v1\controllers;

use app\models\UnitPosition;
use yii\data\ActiveDataProvider;

/**
 * Class UnitPositionController
 *
 * @link http://j96177ni.beget.tech/yii2basic/web/api/unit-position
 */
class UnitPositionController extends RestController
{
    /**
     * @var UnitPosition
     */
    public $modelClass = UnitPosition::class;

    public function actions()
    {
        $parentActions = parent::actions();
        $parentActions['index']['prepareDataProvider'] = [$this, 'prepareDataProvider'];

        return $parentActions;
    }

    public function prepareDataProvider()
    {
        $params = \Yii::$app->request->queryParams;
        $query = $this->modelClass::find()->joinWith('unit')->orderBy('name');
        if (!empty($params['pers_id'])) {
            $query->andWhere(['pers_id' => array_map('intval', explode(',', $params['pers_id']))]);
        }

        return new ActiveDataProvider([
            'query' => $query,
        ]);
    }
}
