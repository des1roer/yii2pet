<?php

namespace app\modules\api\modules\v1\controllers;

use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\rest\ActiveController;
use yii\web\Response;

class RestController extends ActiveController
{
    public $allowedActions = ['index', 'view', 'update', 'create', 'delete'];
}