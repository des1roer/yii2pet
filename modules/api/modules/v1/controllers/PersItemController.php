<?php

namespace app\modules\api\modules\v1\controllers;

use app\models\PersItem;
use app\modules\api\modules\v1\actions\CreateOrUpdateAction;

/**
 * Class PersItemController
 *
 * @link http://j96177ni.beget.tech/yii2basic/web/api/pers
 */
class PersItemController extends RestController
{
    public $modelClass = PersItem::class;

    public function actions()
    {
        $actions = parent::actions();
        $actions['create'] =
            [
                'class' => CreateOrUpdateAction::class,
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
                'scenario' => $this->createScenario,
            ];

        return $actions;
    }
}
