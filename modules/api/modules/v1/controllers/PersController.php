<?php

namespace app\modules\api\modules\v1\controllers;

use app\models\Pers;

/**
 * Class PersController
 *
 * @link http://j96177ni.beget.tech/yii2basic/web/api/pers
 */
class PersController extends RestController
{
    public $modelClass = Pers::class;
}
