<?php

namespace app\modules\api\modules\v1\controllers;

use app\models\Assorty;

class AssortyController extends RestController
{
    public $modelClass = Assorty::class;
}
