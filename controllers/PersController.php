<?php

namespace app\controllers;

use app\models\Pers;
use app\models\PersSearch;
use app\models\PersUnit;
use app\models\Position;
use app\models\Unit;
use app\models\UnitPosition;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\ActiveRecord;
use yii\filters\{
    AccessControl, VerbFilter
};
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * PersController implements the CRUD actions for Pers model.
 */
class PersController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }


    public function actionIndex()
    {
        return $this->render('/game/pers', [
            'data' => Pers::getRaces()
        ]);
    }

    /**
     * @see views/pers/admin.php
     */
    public function actionAdmin()
    {
        $searchModel = new PersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'races' => \yii\helpers\ArrayHelper::map(Pers::getRaces(), 'id', 'name'),
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * Displays a single Pers model.
     *
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Finds the Pers model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     * @return Pers the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Pers::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Creates a new Pers model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Pers();

        if ($model->load($post = Yii::$app->request->post()) && $model->save()) {
            if (!empty($post['pos'])) {
                $this->saveSubModel($model, $post);
            }

            return $this->redirect(['admin']);
        } else {
            return $this->render('create', [
                'model' => $model,
                'positions' => Position::find()->all(),
                'units' => ArrayHelper::map(Unit::find()->all(), 'id', 'name'),
            ]);
        }
    }

    private function saveSubModel($model, $post)
    {
        $positions = array_filter($post['pos']);
        /** @var ActiveRecord $persModel */
        $persModel = UnitPosition::class;

        foreach ($positions as $id => $value) {
            $subModel = $persModel::findOne([
                'position_id' => $id,
                'pers_id' => $model->id,
            ]);
            if ($subModel) {
                $subModel->unit_id = $value;
            } else {
                $subModel = new $persModel([
                    'position_id' => $id,
                    'pers_id' => $model->id,
                    'unit_id' => $value,
                ]);
            }
            $subModel->save();
        }
    }

    /**
     * Updates an existing Pers model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if ($model->load($post = Yii::$app->request->post()) && $model->save()) {
            if (!empty($post['pos'])) {
                $this->saveSubModel($model, $post);
            }

            return $this->redirect(['admin']);
        } else {
            return $this->render('update', [
                'model' => $model,
                'positions' => Position::find()->all(),
                'units' => ArrayHelper::map(Unit::find()->all(), 'id', 'name'),
                /** @see \app\models\Pers::getPersUnits */
                'persUnits' => $model->persUnits,
            ]);
        }
    }

    /**
     * Deletes an existing Pers model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['admin']);
    }
}
