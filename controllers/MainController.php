<?php

namespace app\controllers;

use Yii;
use yii\console\controllers\MigrateController;
use yii\web\Controller;

class MainController extends Controller
{
    /**
     * @throws \yii\base\InvalidRouteException
     * @throws \yii\console\Exception
     */
    public function actionIndex()
    {
        if (!defined('STDOUT')) {
            define('STDOUT', fopen('php://stdout', 'w'));
        }

        $migration = new MigrateController('migrate', Yii::$app);
        $migration->runAction('up', ['migrationPath' => '@app/migrations', 'interactive' => false]);
        //migration command end

        echo '<br>ok';
    }

    /**
     * @throws \yii\base\InvalidRouteException
     * @throws \yii\console\Exception
     */
    public function actionFresh()
    {
        if (!defined('STDOUT')) {
            define('STDOUT', fopen('php://stdout', 'w'));
        }

        $migration = new MigrateController('migrate', Yii::$app);
        $migration->runAction('fresh', ['migrationPath' => '@app/migrations', 'interactive' => false]);
        //migration command end

        echo '<br>ok';
    }
}