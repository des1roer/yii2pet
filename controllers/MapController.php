<?php

namespace app\controllers;

use app\models\Shop;
use PHPUnit\Runner\Exception;
use Yii;
use app\models\Map;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * MapController implements the CRUD actions for Map model.
 */
class MapController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $data = Map::find()
            ->asArray()
            ->all();
        $coords = [];

        if ($data) {
            for ($i = 0, $cnt = count($data); $i < $cnt; $i++) {
                $coords[$i] = json_decode($data[$i]['coords']);
                $coords[$i] = ArrayHelper::toArray($coords[$i]);
                $coords[$i]['properties']['isEnd'] =  $data[$i]['is_end'];
                $coords[$i]['properties']['level'] =  $data[$i]['level'];
                if ($data[$i]['model_id']) {
                    $coords[$i]['properties']['url'] =  Map::getUrl($data[$i]['model'], $data[$i]['model_id']);
                }
            }

            $startPosition = min(ArrayHelper::getColumn($data, 'id'));
            $minLevel = min(ArrayHelper::getColumn($data, 'level'));
            $maxLevel = max(ArrayHelper::getColumn($data, 'level'));
        }

        return $this->render('/game/map', [
            'data' => $coords,
            'startPosition' => $startPosition ?? 0,
            'minLevel' => $minLevel ?? 0,
            'maxLevel' => $maxLevel ?? 0,
        ]);
    }


    /**
     * Lists all Map models.
     *
     * @return mixed
     */
    public function actionAdmin()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Map::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param array $size
     * @return string
     */
    public function actionGenerate(array $size = [10, 10])
    {
        Map::deleteAll();
        for ($x = 1; $x <= $size[0]; $x++) {
            for ($y = 1; $y <= $size[0]; $y++) {
                Map::generateTile($y);
            }
        }
        $ids = Map::find()->select(new Expression('max(id)'))->groupBy('level')->column();
        Map::updateAll(['is_end' => 1], ['id' => $ids]);

        return $this->actionIndex();
    }

    /**
     * Displays a single Map model.
     *
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Map model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Map();
        $post = Yii::$app->request->post();

        if ($post) {
            foreach ($post['Map']['coords'] as $key => $value) {
                $model->coords[$key] = $value;
            }
            unset($post['coords']);
            if ($model->load($post) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Map model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $post = Yii::$app->request->post();
        if ($post) {
            foreach ($post['Map']['coords'] as $key => $value) {
                $model->coords[$key] = $value;
            }
            if ($model->load($post) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionSubcat()
    {
        $data = Yii::$app->request->post();
        if (!empty($data)) {
            echo Json::encode(('app\\models\\'.ucfirst (head($data)))::find()->select('name')->indexBy('id')->asArray()->column());
         }
         die();
    }

    /**
     * Deletes an existing Map model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Map model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     * @return Map the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Map::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
