<?php

use yii\db\Migration;

/**
 * Handles the creation for table `{{%item}}`.
 */
class m180401_122241_create_table_item extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%item}}', [

            'id' => $this->primaryKey()->unsigned()->notNull(),
            'name' => $this->string(20)->notNull(),
            'img' => $this->string(20),
            'cost' => $this->integer(10)->unsigned()->notNull(),

        ]);

        $this->batchInsert('{{%item}}', ['name', 'cost'], [
            ['first', 100],
            ['second', 200],
        ]);

     }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%item}}');
    }
}
