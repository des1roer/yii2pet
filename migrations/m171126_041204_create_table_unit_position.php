<?php

use yii\db\Migration;

/**
 * Handles the creation for table `{{%unit_position}}`.
 */
class m171126_041204_create_table_unit_position extends Migration
{
    const TABLE = '{{%unit_position}}';
    const COLUMN = 'pers_id';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%unit_position}}', [
            'id' => $this->primaryKey()->unsigned()->notNull(),
            'pers_id' => $this->integer(10)->unsigned()->notNull(),
            'unit_id' => $this->integer(10)->unsigned()->notNull(),
            'position_id' => $this->integer(10)->unsigned()->notNull(),
        ]);

        // creates index for column `unit_id`
        $this->createIndex(
            'unit_position_fk1',
            '{{%unit_position}}',
            'unit_id'
        );

        // add foreign key for table `unit`
        $this->addForeignKey(
            'unit_position_fk1',
            '{{%unit_position}}',
            'unit_id',
            '{{%unit}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

        // creates index for column `position_id`
        $this->createIndex(
            'unit_position_fk2',
            '{{%unit_position}}',
            'position_id'
        );

        // add foreign key for table `position`
        $this->addForeignKey(
            'unit_position_fk2',
            '{{%unit_position}}',
            'position_id',
            '{{%position}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

        // creates index for column `pers_id`
        $this->createIndex(
            'unit_position_' . self::COLUMN,
            self::TABLE,
            self::COLUMN
        );

        // add foreign key for table `pers`
        $this->addForeignKey(
            'unit_position_' . self::COLUMN,
            self::TABLE,
            self::COLUMN,
            '{{%pers}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->createIndex('idx_unique_unit_id_transport_id',
            '{{%unit_position}}',
            ['pers_id', 'unit_id', 'position_id'],
            true);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        // drops foreign key for table `unit`
        $this->dropForeignKey(
            'unit_position_fk1',
            '{{%unit_position}}'
        );

        // drops index for column `unit_id`
        $this->dropIndex(
            'unit_position_fk1',
            '{{%unit_position}}'
        );

        // drops foreign key for table `position`
        $this->dropForeignKey(
            'unit_position_fk2',
            '{{%unit_position}}'
        );

        // drops index for column `position_id`
        $this->dropIndex(
            'unit_position_fk2',
            '{{%unit_position}}'
        );

        // drops foreign key for table `pers`
        $this->dropForeignKey(
            'unit_position_' . self::COLUMN,
            self::TABLE
        );

        // drops index for column `pers_id`
        $this->dropIndex(
            'unit_position_' . self::COLUMN,
            self::TABLE
        );

        $this->dropTable('{{%unit_position}}');
    }
}
