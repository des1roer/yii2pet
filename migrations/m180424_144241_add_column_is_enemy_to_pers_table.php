<?php

use yii\db\Migration;

/**
 * Class m180424_144241_add_column_is_enemy_to_pers_table
 */
class m180424_144241_add_column_is_enemy_to_pers_table extends Migration
{
    const TABLE = 'pers';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%' . self::TABLE . '}}', 'is_enemy', $this->smallInteger()->defaultValue(1)->unsigned());
        $this->update('{{%' . self::TABLE . '}}', ['is_enemy' => 0], ['name' => 'mypers']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%' . self::TABLE . '}}', 'is_enemy');
    }
}
