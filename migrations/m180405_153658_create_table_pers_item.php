<?php

use yii\db\Migration;

/**
 * Handles the creation for table `{{%pers_item}}`.
 */
class m180405_153658_create_table_pers_item extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%pers_item}}', [

            'id' => $this->primaryKey()->unsigned()->notNull(),
            'pers_id' => $this->integer(10)->unsigned()->notNull(),
            'item_id' => $this->integer(10)->unsigned()->notNull(),
            'count' => $this->integer(10)->defaultValue(0)->unsigned()->notNull(),
        ]);
 
        // creates index for column `pers_id`
        $this->createIndex(
            'pers_item_fk1',
            '{{%pers_item}}',
            'pers_id'
        );

        // add foreign key for table `pers`
        $this->addForeignKey(
            'pers_item_fk1',
            '{{%pers_item}}',
            'pers_id',
            '{{%pers}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

        // creates index for column `item_id`
        $this->createIndex(
            'pers_item_fk2',
            '{{%pers_item}}',
            'item_id'
        );

        // add foreign key for table `item`
        $this->addForeignKey(
            'pers_item_fk2',
            '{{%pers_item}}',
            'item_id',
            '{{%item}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->createIndex('idx_unique_pers_id_item_id',
            '{{%pers_item}}',
            ['pers_id', 'item_id'],
            true);


        $this->batchInsert('{{%pers_item}}', ['pers_id', 'item_id', 'count'], [
            [1, 1, 10],
            [1, 2, 10],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        // drops foreign key for table `pers`
        $this->dropForeignKey(
            'pers_item_fk1',
            '{{%pers_item}}'
        );

        // drops index for column `pers_id`
        $this->dropIndex(
            'pers_item_fk1',
            '{{%pers_item}}'
        );

        // drops foreign key for table `item`
        $this->dropForeignKey(
            'pers_item_fk2',
            '{{%pers_item}}'
        );

        // drops index for column `item_id`
        $this->dropIndex(
            'pers_item_fk2',
            '{{%pers_item}}'
        );

        $this->dropIndex(
            'idx_unique_pers_id_item_id',
            '{{%page_btn}}'
        );

        $this->dropTable('{{%pers_item}}');
    }
}
