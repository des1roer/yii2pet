<?php

use yii\db\Migration;

/**
 * Class m180403_153944_ins_pers_to_user
 */
class m180403_153944_ins_pers_to_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->batchInsert('{{%user_pers}}', ['user_id', 'pers_id'], [
            [1, 2],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('{{%user_pers}}', ['user_id' => 1, 'pers_id' => 2]);

    }
}
