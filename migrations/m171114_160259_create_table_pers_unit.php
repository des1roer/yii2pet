<?php

use yii\db\Migration;

/**
 * Handles the creation for table `{{%pers_unit}}`.
 */
class m171114_160259_create_table_pers_unit extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%pers_unit}}', [
            'id' => $this->primaryKey()->unsigned()->notNull(),
            'pers_id' => $this->integer(10)->unsigned()->notNull(),
            'unit_id' => $this->integer(10)->unsigned()->notNull(),
            'is_active' => $this->smallInteger()->defaultValue(0)->unsigned(),
        ]);

        // creates index for column `pers_id`
        $this->createIndex(
            'pers_unit_fk1',
            '{{%pers_unit}}',
            'pers_id'
        );

        // add foreign key for table `pers`
        $this->addForeignKey(
            'pers_unit_fk1',
            '{{%pers_unit}}',
            'pers_id',
            '{{%pers}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

        // creates index for column `unit_id`
        $this->createIndex(
            'pers_unit_fk2',
            '{{%pers_unit}}',
            'unit_id'
        );

        // add foreign key for table `unit`
        $this->addForeignKey(
            'pers_unit_fk2',
            '{{%pers_unit}}',
            'unit_id',
            '{{%unit}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->createIndex('idx_unique_pers_id_unit_id',
            '{{%pers_unit}}',
            ['pers_id', 'unit_id'],
            true);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        // drops foreign key for table `pers`
        $this->dropForeignKey(
            'pers_unit_fk1',
            '{{%pers_unit}}'
        );

        // drops index for column `pers_id`
        $this->dropIndex(
            'pers_unit_fk1',
            '{{%pers_unit}}'
        );

        // drops foreign key for table `unit`
        $this->dropForeignKey(
            'pers_unit_fk2',
            '{{%pers_unit}}'
        );

        // drops index for column `unit_id`
        $this->dropIndex(
            'pers_unit_fk2',
            '{{%pers_unit}}'
        );

        $this->dropIndex(
            'idx_unique_pers_id_unit_id',
            '{{%pers_unit}}'
        );

        $this->dropTable('{{%pers_unit}}');
    }
}
