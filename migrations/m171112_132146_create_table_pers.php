<?php

use yii\db\Migration;

/**
 * Handles the creation for table `{{%pers}}`.
 */
class m171112_132146_create_table_pers extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%pers}}', [

            'id' => $this->primaryKey()->unsigned()->notNull(),
            'name' => $this->string(255)->unique()->notNull(),
            'lvl' => $this->integer(10)->unsigned()->notNull()->defaultValue(1),
            'money' => $this->integer(10)->unsigned()->notNull(),
            'race_id' => $this->integer(10)->unsigned()->notNull(),

        ]);

        // creates index for column `race_id`
        $this->createIndex(
            'pers_fk1',
            '{{%pers}}',
            'race_id'
        );

        // add foreign key for table `race`
        $this->addForeignKey(
            'pers_fk1',
            '{{%pers}}',
            'race_id',
            '{{%race}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->batchInsert('{{%pers}}', ['name', 'lvl', 'money', 'race_id'], [
            ['enemy', 1, 0, 1],
            ['mypers', 2, 0, 1],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        // drops foreign key for table `race`
        $this->dropForeignKey(
            'pers_fk1',
            '{{%pers}}'
        );

        // drops index for column `race_id`
        $this->dropIndex(
            'pers_fk1',
            '{{%pers}}'
        );

        $this->dropTable('{{%pers}}');
    }
}
