<?php

use yii\db\Migration;

/**
 * Handles the creation for table `{{%page_btn}}`.
 */
class m171203_054132_create_table_page_btn extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%page_btn}}', [

            'id' => $this->primaryKey()->unsigned()->notNull(),
            'page_id' => $this->integer(10)->unsigned()->notNull(),
            'btn_id' => $this->integer(10)->unsigned()->notNull(),

        ]);

        // creates index for column `btn_id`
        $this->createIndex(
            'page_btn_fk1',
            '{{%page_btn}}',
            'btn_id'
        );

        // add foreign key for table `btn`
        $this->addForeignKey(
            'page_btn_fk1',
            '{{%page_btn}}',
            'btn_id',
            '{{%btn}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

        // creates index for column `page_id`
        $this->createIndex(
            'page_btn_fk2',
            '{{%page_btn}}',
            'page_id'
        );

        // add foreign key for table `page`
        $this->addForeignKey(
            'page_btn_fk2',
            '{{%page_btn}}',
            'page_id',
            '{{%page}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->createIndex('idx_unique_page_id_btn_id',
            '{{%page_btn}}',
            ['page_id', 'btn_id'],
            true);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        // drops foreign key for table `btn`
        $this->dropForeignKey(
            'page_btn_fk1',
            '{{%page_btn}}'
        );

        // drops index for column `btn_id`
        $this->dropIndex(
            'page_btn_fk1',
            '{{%page_btn}}'
        );

        // drops foreign key for table `page`
        $this->dropForeignKey(
            'page_btn_fk2',
            '{{%page_btn}}'
        );

        // drops index for column `page_id`
        $this->dropIndex(
            'page_btn_fk2',
            '{{%page_btn}}'
        );

        $this->dropIndex(
            'idx_unique_page_id_btn_id',
            '{{%page_btn}}'
        );

        $this->dropTable('{{%page_btn}}');
    }
}
