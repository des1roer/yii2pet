<?php

use yii\db\Migration;

/**
 * Handles the creation for table `{{%unit}}`.
 */
class m171113_160259_create_table_unit extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%unit}}', [

            'id' => $this->primaryKey()->unsigned()->notNull(),
            'name' => $this->string(255)->unique()->notNull(),
            'hp' => $this->integer(10)->unsigned()->notNull()->defaultValue(100),
            'atk' => $this->integer(10)->unsigned()->notNull()->defaultValue(10),
            'img' => $this->string(255),
            'race_id' => $this->integer(10)->unsigned()->notNull(),
            'atk_param' => $this->string(255),
            'parent_id' => $this->integer(10)->unsigned(),
        ]);

        // creates index for column `race_id`
        $this->createIndex(
            'unit_fk1',
            '{{%unit}}',
            'race_id'
        );

        // add foreign key for table `race`
        $this->addForeignKey(
            'unit_fk1',
            '{{%unit}}',
            'race_id',
            '{{%race}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

        // add foreign key for table `unit`
        $this->addForeignKey(
            'unit_fk2',
            '{{%unit}}',
            'parent_id',
            '{{%unit}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->batchInsert('{{%unit}}', ['id', 'name', 'hp', 'atk', 'img', 'race_id'], [
            ['1', '1st', '100', '10', 'https://pp.userapi.com/c834204/v834204101/2afb9/luN2Yf_xm2w.jpg', '1'],
            ['2', '2nd', '100', '10', 'https://pp.userapi.com/c834204/v834204101/2afc2/-Gx7DtwI9YI.jpg', '1']]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        // drops foreign key for table `race`
        $this->dropForeignKey(
            'unit_fk1',
            '{{%unit}}'
        );

        // drops index for column `race_id`
        $this->dropIndex(
            'unit_fk1',
            '{{%unit}}'
        );

        $this->dropTable('{{%unit}}');
    }
}
