<?php

use yii\db\Migration;

/**
 * Class m180422_163700_add_column_is_end_to_map
 */
class m180422_163700_add_column_is_end_to_map extends Migration
{
    const TABLE = 'map';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%' . self::TABLE . '}}', 'is_end', $this->smallInteger()->defaultValue(0)->unsigned());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%' . self::TABLE . '}}', 'is_end');
    }
}
