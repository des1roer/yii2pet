<?php

use yii\db\Migration;

/**
 * Handles the creation for table `{{%tile_type}}`.
 */
class m180329_161016_create_table_tile_type extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%tile_type}}', [
            'id' => $this->primaryKey()->unsigned()->notNull(),
            'name' => $this->text(255)->notNull(),
            'description' => $this->text(),
        ]);

        $this->batchInsert('{{%tile_type}}', ['name'], [
            ['Базовая'],
            ['Горы'],
            ['Река']
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%tile_type}}');
    }
}
