<?php

use yii\db\Migration;

/**
 * Class m180403_155416_ins_pers_unit
 */
class m180403_155416_ins_pers_unit extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->batchInsert('{{%unit_position}}', ['pers_id', 'unit_id', 'position_id'], [
            [1, 1, 1],
            [1, 1, 2],
            [1, 1, 3],
            [2, 1, 1],
            [2, 1, 2],
            [2, 1, 3],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        \app\models\UnitPosition::deleteAll();
    }
}
