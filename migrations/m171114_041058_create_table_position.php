<?php

use yii\db\Migration;

/**
 * Handles the creation for table `{{%position}}`.
 */
class m171114_041058_create_table_position extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%position}}', [

            'id' => $this->primaryKey()->unsigned()->notNull(),
            'name' => $this->string(255)->notNull(),
            'alias' => $this->string(255)->notNull(),
        ]);

        $this->batchInsert('{{%position}}', ['id', 'name', 'alias'], [
            ['1', 'Передний ближный', 'firstClose'],
            ['2', 'Задний ближний', 'lastClose'],
            ['3', 'Передний дальний', 'firstFar'],
            ['4', 'Задний дальний', 'lastFar'],
            ['5', 'Большое существо', 'big']]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%position}}');
    }
}
