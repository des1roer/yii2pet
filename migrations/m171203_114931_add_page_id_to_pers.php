<?php

use yii\db\Migration;

/**
 * Class m171203_114931_add_page_id_to_pers
 */
class m171203_114931_add_page_id_to_pers extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('{{%pers}}', 'page_id', $this->integer(10)->unsigned());

        // creates index for column `page_id`
        $this->createIndex(
            'pers_fk2',
            '{{%pers}}',
            'page_id'
        );

        // add foreign key for table `page`
        $this->addForeignKey(
            'pers_fk2',
            '{{%pers}}',
            'page_id',
            '{{%page}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'pers_fk2',
            '{{%pers}}'
        );

        $this->dropColumn('{{%pers}}', 'page_id');
    }
}
