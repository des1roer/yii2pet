<?php

use yii\db\Migration;

/**
 * Class m180424_143307_add_columns_to_map
 */
class m180424_143307_add_columns_to_map extends Migration
{
    const TABLE = 'map';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%' . self::TABLE . '}}',  'img', $this->string(255));
        $this->addColumn('{{%' . self::TABLE . '}}',  'model', $this->string(255));
        $this->addColumn('{{%' . self::TABLE . '}}',  'model_id', $this->integer(10));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%' . self::TABLE . '}}', 'model_id');
        $this->dropColumn('{{%' . self::TABLE . '}}', 'model');
        $this->dropColumn('{{%' . self::TABLE . '}}', 'img');
    }
}
