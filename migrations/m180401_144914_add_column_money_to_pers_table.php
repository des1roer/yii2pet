<?php

use yii\db\Migration;

/**
 * Class m180401_144914_add_column_money_to_pers_table
 */
class m180401_144914_add_column_money_to_pers_table extends Migration
{
    const TABLE = 'user';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%' . self::TABLE . '}}', 'money', $this->integer(10)->unsigned());
        $this->update('{{%' . self::TABLE . '}}', ['money' => 1000], ['username' => 'neo']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%' . self::TABLE . '}}', 'money');
    }
}
