<?php

use yii\db\Migration;

/**
 * Class m180329_161148_add_column_map_id_to_user_table
 */
class m180329_161148_add_column_map_id_to_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%pers}}', 'map_id', $this->integer(10)->unsigned());

        // creates index for column `page_id`
        $this->createIndex(
            'pers_fk3',
            '{{%pers}}',
            'map_id'
        );

        // add foreign key for table `page`
        $this->addForeignKey(
            'pers_fk3',
            '{{%pers}}',
            'map_id',
            '{{%page}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'pers_fk3',
            '{{%pers}}'
        );

        $this->dropColumn('{{%pers}}', 'map_id');
    }
}
