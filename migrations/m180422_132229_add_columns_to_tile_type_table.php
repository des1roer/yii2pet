<?php

use yii\db\Migration;

/**
 * Class m180422_132229_add_columns_to_tyle_table
 */
class m180422_132229_add_columns_to_tile_type_table extends Migration
{
    const TABLE = 'tile_type';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%' . self::TABLE . '}}', 'can_go', $this->smallInteger()->defaultValue(1)->unsigned());
        $this->addColumn('{{%' . self::TABLE . '}}', 'is_specific', $this->smallInteger()->defaultValue(0)->unsigned());

        $this->batchInsert('{{%' . self::TABLE . '}}', ['name', 'is_specific'], [
            ['Город', 1],
            ['Рынок', 1],
        ]);

        $this->update('{{%' . self::TABLE . '}}', ['can_go' => 0], [
            'name' => ['Горы', 'Река']
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%' . self::TABLE . '}}', 'can_go');
        $this->dropColumn('{{%' . self::TABLE . '}}', 'is_specific');

        $this->delete('{{%' . self::TABLE . '}}', ['name' => [
            'Город', 'Рынок'
        ]]);
    }
}
