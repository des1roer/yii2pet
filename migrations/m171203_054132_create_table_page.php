<?php

use yii\db\Migration;

/**
 * Handles the creation for table `{{%page}}`.
 */
class m171203_054132_create_table_page extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%page}}', [
            'id' => $this->primaryKey()->unsigned()->notNull(),
            'name' => $this->string(255)->unique()->notNull(),
            'txt' => $this->string(1023)->notNull(),
            'link' => $this->string(255),
            'img' => $this->string(255),
            'type_id' => $this->integer(10)->unsigned()->notNull(),
            'parent_id' => $this->integer(10)->unsigned(),
        ]);

        // creates index for column `type_id`
        $this->createIndex(
            'page_fk1',
            '{{%page}}',
            'type_id'
        );

        // add foreign key for table `page_type`
        $this->addForeignKey(
            'page_fk1',
            '{{%page}}',
            'type_id',
            '{{%page_type}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

        // add foreign key for table `page`
        $this->addForeignKey(
            'page_fk2',
            '{{%page}}',
            'parent_id',
            '{{%page}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        // drops foreign key for table `page_type`
        $this->dropForeignKey(
            'page_fk1',
            '{{%page}}'
        );

        // drops index for column `type_id`
        $this->dropIndex(
            'page_fk1',
            '{{%page}}'
        );

        $this->dropTable('{{%page}}');
    }
}
