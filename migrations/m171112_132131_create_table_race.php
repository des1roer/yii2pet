<?php

use yii\db\Migration;

/**
 * Handles the creation for table `{{%race}}`.
 */
class m171112_132131_create_table_race extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%race}}', [
            'id' => $this->primaryKey()->unsigned()->notNull(),
            'name' => $this->string(255)->unique()->notNull(),
            'is_active' => $this->integer()->unsigned()->notNull()->defaultValue(1),
            'img' => $this->string(255),
        ]);

        $this->batchInsert('{{%race}}', ['name', 'img'], [
            ['человек', 'https://pp.userapi.com/c636419/v636419756/6199b/BM01lPeewGg.jpg'],
            ['тролль', 'https://pp.userapi.com/c824409/v824409088/37078/cCr0MX0k-Ag.jpg'],
            ['эльф', 'https://pp.userapi.com/c638918/v638918044/3bd80/xHprKq4THs0.jpg']
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%race}}');
    }
}
