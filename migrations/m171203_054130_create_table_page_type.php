<?php

use yii\db\Migration;

/**
 * Handles the creation for table `{{%page_type}}`.
 */
class m171203_054130_create_table_page_type extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%page_type}}', [

            'id' => $this->primaryKey()->unsigned()->notNull(),
            'name' => $this->string(255)->notNull(),
        ]);

        $this->batchInsert('{{%page_type}}', ['id', 'name'], [
            ['1', 'Обычная страница'],
            ['2', 'Магазин']]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%page_type}}');
    }
}
