<?php

use yii\db\Migration;

/**
 * Handles the creation for table `{{%transport}}`.
 */
class m171119_070019_create_table_transport extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%transport}}', [
            'id' => $this->primaryKey()->unsigned()->notNull(),
            'name' => $this->string(255)->unique()->notNull(),
            'lvl' => $this->integer(10)->unsigned()->notNull()->defaultValue(1),
            'price' => $this->integer(10)->unsigned()->notNull()->defaultValue(100),
            'description' => $this->string(511),
            'img' => $this->string(255),
            'params' => $this->string(),
        ]);

        $this->batchInsert('{{%transport}}', ['name', 'img'], [
            ['Мастодонт', 'https://pre00.deviantart.net/5400/th/pre/i/2011/019/c/6/fantasy_transport_by_beaulamb1992-d37jzyo.jpg'],
            ['Жуть', 'https://i.pinimg.com/736x/cb/6c/e1/cb6ce155f1ce0b014e3a2834a4dcb452--character-concept-character-design.jpg'],
            ['Карета', 'https://cdn.pixabay.com/photo/2017/09/19/13/54/transport-2765317_960_720.jpg'],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%transport}}');
    }
}
