<?php

use yii\db\Migration;

/**
 * Handles the creation for table `{{%btn}}`.
 */
class m171203_054132_create_table_btn extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%btn}}', [

            'id' => $this->primaryKey()->unsigned()->notNull(),
            'link' => $this->string(255),
            'sort' => $this->integer(10)->unsigned()->notNull()->defaultValue(1),
            'img' => $this->string(255),

        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%btn}}');
    }
}
