<?php

use yii\db\Migration;

/**
 * Handles the creation for table `{{%unit_transport}}`.
 */
class m171119_070028_create_table_unit_transport extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%unit_transport}}', [
            'id' => $this->primaryKey()->unsigned()->notNull(),
            'unit_id' => $this->integer(10)->unsigned()->notNull(),
            'transport_id' => $this->integer(10)->unsigned()->notNull(),

        ]);

        // creates index for column `unit_id`
        $this->createIndex(
            'unit_transport_fk1',
            '{{%unit_transport}}',
            'unit_id'
        );

        // add foreign key for table `unit`
        $this->addForeignKey(
            'unit_transport_fk1',
            '{{%unit_transport}}',
            'unit_id',
            '{{%unit}}',
            'id',
            'CASCADE'
        );

        // creates index for column `transport_id`
        $this->createIndex(
            'unit_transport_fk2',
            '{{%unit_transport}}',
            'transport_id'
        );

        // add foreign key for table `transport`
        $this->addForeignKey(
            'unit_transport_fk2',
            '{{%unit_transport}}',
            'transport_id',
            '{{%transport}}',
            'id',
            'CASCADE'
        );

        $this->createIndex('idx_unique_unit_id_transport_id',
            '{{%unit_transport}}',
            ['unit_id', 'transport_id'],
            true);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        // drops foreign key for table `unit`
        $this->dropForeignKey(
            'unit_transport_fk1',
            '{{%unit_transport}}'
        );

        // drops index for column `unit_id`
        $this->dropIndex(
            'unit_transport_fk1',
            '{{%unit_transport}}'
        );

        // drops foreign key for table `transport`
        $this->dropForeignKey(
            'unit_transport_fk2',
            '{{%unit_transport}}'
        );

        // drops index for column `transport_id`
        $this->dropIndex(
            'unit_transport_fk2',
            '{{%unit_transport}}'
        );

        $this->dropIndex(
            'idx_unique_unit_id_transport_id',
            '{{%unit_transport}}'
        );

        $this->dropTable('{{%unit_transport}}');
    }
}
