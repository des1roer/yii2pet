<?php

use yii\db\Migration;

/**
 * Class m180422_083958_add_columns_to_map
 */
class m180422_083958_add_columns_to_map extends Migration
{
    const TABLE = 'map';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%' . self::TABLE . '}}', 'level', $this->integer(10)->defaultValue(1)->unsigned());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%' . self::TABLE . '}}', 'level');
    }
}
