<?php

use yii\db\Migration;

/**
 * Handles the creation for table `{{%map}}`.
 */
class m180329_161024_create_table_map extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%map}}', [

            'id' => $this->primaryKey()->unsigned()->notNull(),
            'coords' => $this->text()->notNull(),
            'title' => $this->text()->notNull(),
            'tile_type_id' => $this->integer(10)->unsigned()->notNull(),

        ]);
 
        // creates index for column `tile_type_id`
        $this->createIndex(
            'map_fk1',
            '{{%map}}',
            'tile_type_id'
        );

        // add foreign key for table `tile_type`
        $this->addForeignKey(
            'map_fk1',
            '{{%map}}',
            'tile_type_id',
            '{{%tile_type}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        // drops foreign key for table `tile_type`
        $this->dropForeignKey(
            'map_fk1',
            '{{%map}}'
        );

        // drops index for column `tile_type_id`
        $this->dropIndex(
            'map_fk1',
            '{{%map}}'
        );

        $this->dropTable('{{%map}}');
    }
}
