!(function () {
    'use strict';

    var vm = new Vue({
        el: '#app',
        data: {
            rows: data,
            unit: unit
        },
        computed: {},
        mounted: function () {
            c(this.rows);
            $('#gallery').photobox('a', {time: 0}, callback);
        },
        methods: {
            change: function (data) {
                c(data)
            },
            prev: function (data) {
                if (this.unit.parent_id) {
                    window.location = this.unit.parent_id;
                }
            },
            next: function (data) {
                if (this.unit.child_id) {
                    window.location = this.unit.child_id;
                }
            }
        }
    });

    function callback() {
        c(this)
    }

})();