!(function () {
    'use strict';

    var nullStruct = {
        big: '',
        firstClose: '',
        lastClose: '',
        firstFar: '',
        lastFar: '',
    };

    var vm = new Vue({
        el: '#app',
        data: {
            loger: '',
            rows: data,
            units: null,
            webPath: webPath,
            positions: {
                my: {
                    big: '',
                    firstClose: '',
                    lastClose: '',
                    firstFar: '',
                    lastFar: '',
                },
                enemy: {
                    big: '',
                    firstClose: '',
                    lastClose: '',
                    firstFar: '',
                    lastFar: '',
                }
            },
            hp: {
                'my': 0,
                'enemy': 0,
            },
            show: true,
        },
        computed: {},
        mounted: function () {
            var self = this;

            $.ajax({
                url: this.webPath + "/api/unit-position/?expand=unit&pers_id=" + self.rows.pers.id + ',' + self.rows.enemy.id,
                type: 'GET',
                data: {},
                success: function (data) {
                    self.units = data;
                    var key = null;
                    var arr = {
                        'my': self.rows.pers.id,
                        'enemy': self.rows.enemy.id,
                    };

                    for (key in self.units) {
                        var unit = self.units[key].unit,
                            pers_id = self.units[key].pers_id;

                        for (var code in arr) {
                            if (pers_id == arr[code]) {
                                self.hp[code] += unit.hp;
                            }
                        }
                    }

                    var arr = {
                        'my': self.rows.pers.id,
                        'enemy': self.rows.enemy.id,
                    };
                    for (name in nullStruct) {
                        var row = null;
                        for (var code in arr) {
                            if (_.isObject(row = _.findWhere(data, {position_alias: name, pers_id: arr[code]}))) {
                                self.positions[code][name] = row.unit.img;
                            }
                        }
                    }
                    c(self.positions)
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    // alert(errorThrown);
                }
            });
        },
        methods: {
            start: function (data) {
                var self = this, key = null;
                var nick = 'Empty ';
                var arr = {
                    'my': self.rows.pers.id,
                    'enemy': self.rows.enemy.id
                };

                for (key in self.units) {
                    var unit = self.units[key].unit,
                        dmg = Math.round(random(0.9, 1.1) * unit.atk),
                        pers_id = self.units[key].pers_id;

                    for (var code in arr) {
                        if (pers_id != arr[code]) {

                            if (pers_id != self.rows.pers.id) {
                                nick = 'User';
                            } else {
                                nick = 'Empty';
                            }
                            self.loger += nick + ' ' + self.hp[code] + ' - ' + dmg + '</br>'

                            self.hp[code] -= dmg;

                            if (self.hp[code] <= 0) {
                                if (code != 'my') {
                                    nick = 'User';
                                } else {
                                    nick = 'Empty';
                                }

                                self.loger += nick + ' winner</br>';
                                self.show = false;
                                return false;
                            }
                        }
                    }

                    //////////////////////////
                }


                self.loger += '</br>'
            },
        }
    });

    function random(min, max) {
        var rand = min + Math.random() * (max - min);
        return rand;
    }


})();