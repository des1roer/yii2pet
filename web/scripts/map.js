Vue.component('modal', {
  template: '#modal-template',
  methods: {
    write: function () {
      var e = window.event;
      var target = e.target || e.srcElement;
      var popup = document.getElementById('modal-container');
      
      while (target.parentNode && target.tagName.toLowerCase() !== 'body') {
        if (target === popup) {
          //  vm.showModal = false;
          return;
        }
        
        target = target.parentNode;
      }
      
      vm.showModal = false;
    }
  }
});

var vm = new Vue({
  el: '#app',
  data: {
    cols: {
      name: 'Name',
      season: 'Season',
      series: 'Series',
      url: 'URL'
    },
    add_row: {},
    rows: {},
    showModal: false,
    key: '840802a3-cade-4565-bd66-213f7c5272ae',
    auth: '',
    loader: true,
    user_id: '',
    isMobile: navigator.userAgent.match(/Android/i),
    user_agent: navigator.userAgent,
    msg: '',
    elem: null,
    is_specific: false,
    base: null,
    url: null
  },
  mounted: function () {
  
  },
  methods: {
    test: function () {
    
    },
    edit: function () {
      location.href = 'map/update/' + vm.elem
    },
  }
});

window.addEventListener('keydown', function (e) {
  if (e.keyCode === 27) { //esc
    vm.showModal = false;
  }
});
// end Vue


//set up styles & data
var highlightStyle = {
  weight: 3,
  color: '#3B555C',
  dashArray: '',
  fillOpacity: 0.6
};

var states = data;

//define map
var map = L.map('map', {
  center: [0.005, 0.01],
  zoom: 15
});

function fitBounds(bounds) {
  map.fitBounds(bounds);
}

// function onMapClick(e) {
//     vm.msg = e.target.feature.properties.msg;
//     vm.showModal = true;
//     // alert("You clicked the map at " + e.latlng);
// }

function onMapClick(e) {
  vm.msg = e.target.feature.properties.id;
  vm.showModal = true;
  vm.elem = e.target.feature.properties.id;
  // c(e.target.feature.properties);
  vm.is_specific = e.target.feature.properties.is_specific === 1;
  if (e.target.feature.properties.url) {
    vm.url = e.target.feature.properties.url;
  }

  $.ajax({
    url: webPath + "/api/map/step",
    type: 'POST',
    data: {
      tile_id: start,
      step: e.target.feature.properties.id,
      isStart: vm.base.properties.isStart,
      isEnd: vm.base.properties.isEnd,
      level: vm.base.properties.level,
      minLevel: minLevel,
      maxLevel: maxLevel,
      stepLevel: e.target.feature.properties.level,
      selfNumber: vm.base.properties.number,
      stepNumber: e.target.feature.properties.number
    },
    success: function (data) {
      var data = JSON.parse(data);
      vm.msg += ' ' + data;
      if (data == true) {
        e.target.setStyle({color: "#d632ff"});
        vm.base = e.target.feature;
        start = e.target.feature.properties.id;
        map.eachLayer(function (layer) {
          map.removeLayer(layer);
        });
        initGeo();
      }
    },
    error: function (jqXHR, textStatus, errorThrown) {
      // alert(errorThrown);
    },
    complete: function (data) {
      // c(item)
    }
  });
  
  // states[states.length] = getTile(states.length);
  // console.log(states)
  // map.eachLayer(function (layer) {
  //   map.removeLayer(layer);
  // });
  // L.geoJson(states, {
  //   style: function (feature) {
  //     console.log(feature.properties.atata)
  //     switch (feature.properties.party) {
  //       case 'Republican':
  //         return {color: "#ff0000"};
  //       case 'Democrat':
  //         return {color: "#0000ff"};
  //     }
  //   },
  //   onEachFeature: onEachFeature,
  // }).addTo(map)
  // alert("You clicked the map at " + e.latlng);
}

function onMapClick2(e) {
  // c(e.latlng)
}

function highlightFeature(evt) {
  var feature = evt.target;
  feature.setStyle(highlightStyle);
  if (!L.Browser.ie && !L.Browser.opera) {
    feature.bringToFront();
  }
}

function resetHighlight(evt) {
  statesLayer.resetStyle(evt.target);
}

function popUpFeature(feature, layer) {
  var popupText = "Yo, I'm a <b>" + feature.properties.party + "</b> y'all!<br>";
  layer.bindPopup(popupText);
}

//set up the feature iteration
var onEachFeature = function (feature, layer) {
  // popUpFeature(feature, layer);
  // vm.showModal = true;
  layer.on({
    mouseover: highlightFeature,
    mouseout: resetHighlight,
    click: onMapClick,
    mousemove: onMapClick2,
  });
};
//add the stae layer

function initGeo() {
  statesLayer = L.geoJson(states, {
    style: function (feature) {
      if (feature.properties.id === start) {
        vm.base = feature;
        return {color: "#d632ff"};
      }
    },
    onEachFeature: onEachFeature
  }).addTo(map);
}

initGeo();

function getTile(i) {
  var len = 0.001;
  return {
    "type": "Feature",
    "properties": {
      "party": "Democrat",
      'atata': 'dsfsd'
    },
    "geometry": {
      "type": "Polygon",
      "coordinates": [[
        [i * len, i * len].reverse(),
        [i * len + len, i * len].reverse(),
        [i * len + len, i * len + 2 * (len)].reverse(),
        [i * len, i * len + 2 * (len)].reverse(),
      ]]
    }
  };
}

// document.onclick = function(e) {
//     onClick(e, 'modal-wrapper');
// }
