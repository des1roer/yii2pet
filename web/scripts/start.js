c = function () {
    return console.log.apply(console, arguments);
};

// noinspection JSDuplicatedDeclaration
var vm = new Vue({
    el: '#app',
    data: {
        rows: data,
        val: '',
        url: null,
        name: '',
        user_id: user_id
    },
    computed: {
        hasError: function () {
            return this.nameTrim.length == 0;
        },
        nameTrim: function () {
            return this.name.trim();
        },
    },
    mounted: function () {
        this.id = 0;
        this.val = this.rows[this.id].name;
        this.url = this.rows[this.id].img;
    },
    methods: {
        prev: function () {
            this.change_slide(plus = false);
        },
        next: function () {
            this.change_slide();
        },
        change_slide: function (plus = true) {
            var plus = plus;
            var keys = Object.keys(this.rows);
            var loc = keys.indexOf(this.id.toString());
            loc = (plus) ? loc + 1 : loc - 1;
            if (typeof this.rows[keys[loc]] == 'undefined') {
                if (plus) {
                    loc = 0;
                } else {
                    loc = this.rows.length - 1;
                }
            }

            window.location.hash = keys[loc];
            this.id = keys[loc];
            this.val = this.rows[this.id].name;
            this.url = this.rows[this.id].img;
            this.set_active(this.rows[this.id].id);
        },
        set_active: function (id) {
            c(this.name);
            $.ajax({
                url: "api/user/" + this.user_id + '?scenario=pers',
                type: 'PUT',
                data: {
                    race_id: id,
                    pers_name: this.nameTrim,
                    scenario: 'pers'
                },
                success: function (data) {
                    c(data);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    // alert(errorThrown);
                }
            });
        },
        army: function () {
            this.set_active(this.rows[this.id].id);
            window.location = "unit/army/";
        },
    }
})
