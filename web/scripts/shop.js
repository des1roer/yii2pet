!(function () {
  'use strict';
  
  Vue.component('modal', {
    template: '#modal-template',
    methods: {
      write: function () {
        var e = window.event;
        var target = e.target || e.srcElement;
        var popup = document.getElementById('modal-container');
        
        while (target.parentNode && target.tagName.toLowerCase() !== 'body') {
          if (target === popup) {
            //  vm.showModal = false;
            return;
          }
          
          target = target.parentNode;
        }
        
        vm.showModal = false;
      }
    }
  });
  
  var vm = new Vue({
    el: '#app',
    data: {
      shopItems: shop['assorty'],
      user: user,
      userItems: user['persItems'],
      showModal: false,
      msg: '',
      isBuy: false,
      isSale: false,
      item: null,
      webPath: webPath,
      count: 1
    },
    mounted: function () {
      c(user)
    },
    methods: {
      buyItem: function (data) {
        this.showModal = this.isBuy = true;
        this.isSale = false;
        this.msg = data.name + ' ' + data.cost;
        this.item = data;
      },
      sale: function () {
        this.userItems[this.item.id]['count']--;
        this.user.pers.money = parseInt(this.user.pers.money) + parseInt(this.item.cost);
        this.post();
      },
      saleItem: function (data) {
        data = data['item'];
        this.showModal = this.isSale = true;
        this.isBuy = false;
        this.msg = data.name + ' ' + data.cost;
        this.item = data;
      },
      buy: function () {
        var key = null;
        for (key in this.userItems) {
          if (this.userItems.hasOwnProperty(this.item.id)) {
            this.userItems[this.item.id]['count']++;
            break;
          } else {
            this.userItems[this.item.id] = {
              count: this.count
            };
            this.userItems[this.item.id]['item'] = {
              name: this.item.name,
              cost: this.item.cost
            };
  
          }
        }
        if (this.user.pers.money - this.item.cost >= 0) {
          this.user.pers.money -= this.item.cost;
          this.post();
        } else {
          alert('No money');
        }
      },
      post: function () {
        // c(this.item)
        $.ajax({
          url: this.webPath + "/api/pers-item",
          type: 'POST',
          data: {
            pers_id: this.user['pers'].id,
            item_id: this.item.id,
            count: this.userItems[this.item.id]['count']
          },
          success: function (data) {
            var data = JSON.parse(data);
            if (typeof data.error === 'undefined') {
              // c(data);
            } else {
              // alert(data.error.errorInfo);
            }
          },
          error: function (jqXHR, textStatus, errorThrown) {
            // alert(errorThrown);
          },
          complete: function (data) {
            // c(item)
          }
        });
        if (this.userItems[this.item.id]['count'] === 0) {
          delete this.userItems[this.item.id];
          this.showModal = false;
        }
        $.ajax({
          url: this.webPath + "/api/pers/" + this.user.pers.id,
          type: 'PUT',
          data: {
            money: this.user.pers.money
          },
          success: function (data) {
            // var data = JSON.parse(data);
            if (typeof data.error === 'undefined') {
              // c(data);
            } else {
              // alert(data.error.errorInfo);
            }
          },
          error: function (jqXHR, textStatus, errorThrown) {
            // alert(errorThrown);
          }
        });
        $.ajax({
          url: this.webPath + "/api/assorty/" + this.shopItems[this.item.id].id,
          type: 'PUT',
          data: {
            count: this.shopItems[this.item.id].count
          },
          success: function (data) {
            // var data = JSON.parse(data);
            if (typeof data.error === 'undefined') {
              // c(data);
            } else {
              // alert(data.error.errorInfo);
            }
          },
          error: function (jqXHR, textStatus, errorThrown) {
            // alert(errorThrown);
          }
        });
  
  
        if (this.shopItems.count === 0) {
          delete this.shopItems[this.item.id];
          this.showModal = false;
        } else {
          this.shopItems[this.item.id].count -= 1;
        }
        
      }
    }
  })
  
})();