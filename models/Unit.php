<?php

namespace app\models;

use yii\helpers\Url;

/**
 * This is the model class for table "ds_unit".
 *
 * @property int             $id
 * @property string          $name
 * @property int             $hp
 * @property int             $atk
 * @property string          $img
 * @property int             $race_id
 * @property array           $atk_param
 * @property int             $parent_id
 *
 * @property PersUnit[]      $persUnits
 * @property Race            $race
 * @property Unit            $parent
 * @property Unit[]          $units
 * @property UnitPosition[]  $unitPositions
 * @property Position[]      $positions
 * @property UnitTransport[] $unitTransports
 * @property Transport[]     $transports
 */
class Unit extends \yii\db\ActiveRecord
{
    public $url;
    public $child_id;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%unit}}';
    }

    /**
     * @inheritdoc
     * @return UnitQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new UnitQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'race_id'], 'required'],
            [['hp', 'atk', 'race_id', 'parent_id'], 'integer'],
            [['hp'], 'default', 'value' => 100],
            [['atk'], 'default', 'value' => 10],
            [['name', 'img'], 'string', 'max' => 255],
            [['name', 'parent_id'], 'unique'],
            [['img'], 'unique'],
            [
                ['race_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Race::class,
                'targetAttribute' => ['race_id' => 'id']
            ],
            ['atk_param', 'string'],
            [
                ['parent_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Unit::class,
                'targetAttribute' => ['parent_id' => 'id']
            ],
            ['child_id', 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function fields()
    {
        $fields = parent::fields();
        $fields['url'] = function () {
            return $this->getUrl();
        };
        $fields['child_id'] = function () {
            return $this->getChild() ? $this->getChild()->id : null;
        };

        return $fields;
    }

    /**
     * @inheritdoc
     */
    public function getUrl()
    {
        return Url::toRoute($this->id);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'hp' => 'Hp',
            'atk' => 'Atk',
            'img' => 'Img',
            'race_id' => 'Race ID',
            'atk_param' => 'Atk Param',
            'parent_id' => 'Parent ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPersUnits()
    {
        return $this->hasMany(UnitPosition::class, ['unit_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRace()
    {
        return $this->hasOne(Race::class, ['id' => 'race_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(Unit::class, ['id' => 'parent_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUnits()
    {
        return $this->hasMany(Unit::class, ['parent_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUnitPositions()
    {
        return $this->hasMany(UnitPosition::class, ['unit_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPositions()
    {
        return $this->hasMany(Position::class, ['id' => 'position_id'])
            ->viaTable('ds_unit_position', ['unit_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUnitTransports()
    {
        return $this->hasMany(UnitTransport::class, ['unit_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransports()
    {
        return $this->hasMany(Transport::class, ['id' => 'transport_id'])
            ->viaTable('ds_unit_transport', ['unit_id' => 'id']);
    }

    public function getChild()
    {
        return self::find()->andWhere(['parent_id' => $this->id])->one();
    }
}
