<?php

namespace app\models;

use paulzi\jsonBehavior\JsonBehavior;
use paulzi\jsonBehavior\JsonField;
use paulzi\jsonBehavior\JsonValidator;
use yii\base\InvalidArgumentException;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%map}}".
 *
 * @property int      $id
 * @property string   $coords
 * @property string   $title
 * @property int      $tile_type_id
 * @property int      $level
 * @property int      $is_end
 *
 * @property TileType $tileType
 */
class Map extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%map}}';
    }

    public function behaviors()
    {
        return [
            [
                'class' => JsonBehavior::class,
                'attributes' => ['coords'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['coords', 'title', 'tile_type_id'], 'required'],
            ['tile_type_id', 'default', 'value' => 1],
            ['level', 'default', 'value' => 1],
            ['is_end', 'default', 'value' => 0],
            [['title', 'model'], 'string'],
            [['coords'], JsonValidator::class, 'merge' => true],
            [['coords'], 'filter', 'filter' => function ($value) {
                try {
                    $value = trim(str_replace("'", '"', $value));
                    \yii\helpers\Json::decode($value);

                    return $value;
                } catch (InvalidArgumentException $e) {
                    $this->addError('coords', $e->getMessage());
                    return null;
                }
            }],
            [['tile_type_id', 'level', 'is_end', 'model_id'], 'integer'],
            [
                ['tile_type_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => TileType::class,
                'targetAttribute' => ['tile_type_id' => 'id']
            ],
        ];
    }

    public function afterSave($insert, $changedAttributes)
    {
        $coords = json_decode($this->coords);
        $coords = ArrayHelper::toArray($coords);
        $coords['properties']['id'] = $this->id;
        $coords['properties']['can_go'] = $this->tileType->can_go;
        $coords['properties']['is_specific'] = $this->tileType->is_specific;
        $coords['properties']['level'] = $this->level;
        $coords['properties']['number'] = Map::find()
            ->where(['level' => $this->level])
            ->count();
        // scalar отдает string === нельзя
        $isStart = Map::find()
                ->select(new Expression('min(id)'))
                ->where(['level' => $this->level])
                ->scalar() == $this->id;
        $coords['properties']['isStart'] = $isStart;
        $this->coords = json_encode($coords);
        $this->updateAttributes(['coords']);

        parent::afterSave($insert, $changedAttributes);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'coords' => 'Coords',
            'title' => 'Title',
            'tile_type_id' => 'Tile Type ID',
            'level' => 'level',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTileType()
    {
        return $this->hasOne(TileType::class, ['id' => 'tile_type_id']);
    }

    /**
     * @inheritdoc
     */
    public function isAttributeChanged($name, $identical = true)
    {
        if ($this->$name instanceof JsonField) {
            return (string) $this->$name !== $this->getOldAttribute($name);
        } else {
            return parent::isAttributeChanged($name, $identical);
        }
    }

    /**
     * @inheritdoc
     */
    public function getDirtyAttributes($names = null)
    {
        $result = [];
        $data = parent::getDirtyAttributes($names);
        foreach ($data as $name => $value) {
            if ($value instanceof JsonField) {
                if ((string) $value !== $this->getOldAttribute($name)) {
                    $result[$name] = $value;
                }
            } else {
                $result[$name] = $value;
            }
        }
        return $result;
    }

    public static function generateTile(int $level = 1): int
    {
        $result = 0;
        $model = new self([
            'title' => 'default',
            'tile_type_id' => 1,
            'level' => $level,
        ]);
        $len = 0.001;
        $count = Map::find()->where(['level' => $level])->count();
        $base = 0;
        $shift = 0;
        if ($level % 2 === 0) {
            $shift += $len;
        }
        $upLevel = $level * $len;
        $model->coords = [
            'type' => 'Feature',
            'properties' => [
                'msg' => 'Democrat'
            ],
            'geometry' => [
                'type' => 'Polygon',
                'coordinates' => [
                    [
                        array_reverse([
                            $upLevel + $base * $len,
                            $shift + $count * 2 * $len
                        ]), // левый нижний
                        array_reverse([
                            $upLevel + $base * $len + $len,
                            $shift + $count * 2 * $len
                        ]), // левый верхний
                        array_reverse([
                            $upLevel + $base * $len + $len,
                            $shift + $base * $len + 2 * $len + $count * 2 * $len
                        ]),
                        array_reverse([
                            $upLevel + $base * $len,
                            $shift + $base * $len + 2 * $len + $count * 2 * $len
                        ]),
                    ]
                ]
            ]
        ];
        $model->save();

        return $model->id ?: $result;
    }

    public static function getUrl(string $model, int $id): string
    {
        return $model . '/' . $id;
    }
}
