<?php

namespace app\models;

use yii\db\ActiveQuery;

/**
 * This is the model class for table "{{%user_pers}}".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $pers_id
 *
 * @property Pers    $pers
 * @property User    $user
 */
class UserPers extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName(): string
    {
        return '{{%user_pers}}';
    }

    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [['user_id', 'pers_id'], 'required'],
            [['user_id', 'pers_id'], 'integer'],
            [['pers_id'], 'exist', 'skipOnError' => true, 'targetClass' => Pers::class, 'targetAttribute' => ['pers_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['user_id' => 'id']],
            [['user_id', 'pers_id'], 'unique', 'targetAttribute' => ['user_id', 'pers_id'],
                'message' => 'The combination of User ID and Pers ID has already been taken.'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'pers_id' => 'Pers ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPers(): ActiveQuery
    {
        return $this->hasOne(Pers::class, ['id' => 'pers_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser(): ActiveQuery
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }
}
