<?php

namespace app\models;

/**
 * This is the model class for table "{{%unit_transport}}".
 *
 * @property integer   $id
 * @property integer   $unit_id
 * @property integer   $transport_id
 *
 * @property Unit      $unit
 * @property Transport $transport
 */
class UnitTransport extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%unit_transport}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['unit_id', 'transport_id'], 'required'],
            [['unit_id', 'transport_id'], 'integer'],
            [['unit_id', 'transport_id'], 'unique', 'targetAttribute' => ['unit_id', 'transport_id'], 'message' => 'The combination of Unit ID and Transport ID has already been taken.'],
            [['unit_id'], 'exist', 'skipOnError' => true, 'targetClass' => Unit::class, 'targetAttribute' => ['unit_id' => 'id']],
            [['transport_id'], 'exist', 'skipOnError' => true, 'targetClass' => Transport::class, 'targetAttribute' => ['transport_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'unit_id' => 'Unit ID',
            'transport_id' => 'Transport ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUnit()
    {
        return $this->hasOne(Unit::class, ['id' => 'unit_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransport()
    {
        return $this->hasOne(Transport::class, ['id' => 'transport_id']);
    }
}
