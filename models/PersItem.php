<?php

namespace app\models;

/**
 * This is the model class for table "{{%pers_item}}".
 *
 * @property int $id
 * @property int $pers_id
 * @property int $item_id
 * @property int $count
 *
 * @property Pers $pers
 * @property Item $item
 */
class PersItem extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%pers_item}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pers_id', 'item_id'], 'required'],
            [['pers_id', 'item_id'], 'integer'],
            [['pers_id'], 'unique', 'targetAttribute' => ['pers_id', 'item_id']],
            [
                ['pers_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Pers::class,
                'targetAttribute' => ['pers_id' => 'id']
            ],
            [
                ['item_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Item::class,
                'targetAttribute' => ['item_id' => 'id']
            ],
            ['count', 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'pers_id' => 'Pers ID',
            'item_id' => 'Item ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPers()
    {
        return $this->hasOne(Pers::class, ['id' => 'pers_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItem()
    {
        return $this->hasOne(Item::class, ['id' => 'item_id']);
    }
}
