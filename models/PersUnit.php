<?php

namespace app\models;

/**
 * This is the model class for table "ds_pers_unit".
 *
 * @property int $id
 * @property int $pers_id
 * @property int $unit_id
 *
 * @property Pers $pers
 * @property Unit $unit
 * @property Position $position
 */
class PersUnit extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%pers_unit}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pers_id', 'unit_id', 'position_id'], 'required'],
            [['pers_id', 'unit_id', 'position_id', 'is_active'], 'integer'],
            [
                ['pers_id', 'unit_id', 'position_id'],
                'unique',
                'targetAttribute' => ['pers_id', 'unit_id', 'position_id'],
                'message' => 'The combination of Pers ID, Unit ID and Position ID has already been taken.'
            ],
            [
                ['pers_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Pers::class,
                'targetAttribute' => ['pers_id' => 'id']
            ],
            [
                ['unit_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Unit::class,
                'targetAttribute' => ['unit_id' => 'id']
            ],
            ['is_active', 'default', 'value' => 0],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'pers_id' => 'Pers ID',
            'unit_id' => 'Unit ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPers()
    {
        return $this->hasOne(Pers::class, ['id' => 'pers_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUnit()
    {
        return $this->hasOne(Unit::class, ['id' => 'unit_id']);
    }
}
