<?php

namespace app\models;

/**
 * This is the model class for table "{{%transport}}".
 *
 * @property integer         $id
 * @property string          $name
 * @property integer         $lvl
 * @property integer         $price
 * @property string          $description
 * @property string          $params
 *
 * @property UnitTransport[] $unitTransports
 * @property Unit[]          $units
 */
class Transport extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%transport}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name', 'img'], 'unique'],
            [['lvl', 'price'], 'integer'],
            [['lvl'], 'default', 'value' => 1],
            [['price'], 'default', 'value' => 100],
            [['name', 'params', 'img'], 'string', 'max' => 255],
            [['description'], 'string', 'max' => 511],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'lvl' => 'Lvl',
            'price' => 'Price',
            'description' => 'Description',
            'params' => 'Params',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUnitTransports()
    {
        return $this->hasMany(UnitTransport::class, ['transport_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUnits()
    {
        return $this->hasMany(Unit::class, ['id' => 'unit_id'])
            ->viaTable(UnitTransport::tableName(), ['transport_id' => 'id']);
    }
}
