<?php

namespace app\models;

/**
 * This is the model class for table "{{%position}}".
 *
 * @property integer $id
 * @property string $name
 *
 * @property UnitPosition[] $unitPositions
 * @property Unit[] $units
 */
class Position extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%position}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'alias'], 'required'],
            [['name', 'alias'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'alias' => 'alias'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUnitPositions()
    {
        return $this->hasMany(UnitPosition::class, ['position_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUnits()
    {
        return $this->hasMany(Unit::class, ['id' => 'unit_id'])->viaTable('{{%unit_position}}',
            ['position_id' => 'id']);
    }
}
