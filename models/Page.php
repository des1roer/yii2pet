<?php

namespace app\models;

/**
 * This is the model class for table "{{%page}}".
 *
 * @property integer   $id
 * @property string    $txt
 * @property string    $link
 * @property string    $img
 * @property integer   $type_id
 *
 * @property PageType  $type
 * @property PageBtn[] $pageBtns
 * @property Btn[]     $btns
 */
class Page extends \yii\db\ActiveRecord
{
    /**
     * @var int Ид обычной страницы
     */
    const BASE_TYPE_ID = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%page}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['txt', 'type_id', 'name'], 'required'],
            ['type_id', 'default', 'value' => self::BASE_TYPE_ID],
            [['name'], 'unique'],
            [['type_id'], 'integer'],
            ['link', 'url'],
            [['txt'], 'string', 'max' => 1023],
            [['link', 'img', 'name'], 'string', 'max' => 255],
            [['type_id'], 'exist', 'skipOnError' => true, 'targetClass' => PageType::class,
                'targetAttribute' => ['type_id' => 'id']],
            [['parent_id'], 'exist', 'skipOnError' => true,
                'targetClass' => static::class, 'targetAttribute' => ['parent_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'txt' => 'Txt',
            'link' => 'Link',
            'img' => 'Img',
            'type_id' => 'Тип страницы',
            'parent_id' => 'Предок',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(PageType::class, ['id' => 'type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPageBtns()
    {
        return $this->hasMany(PageBtn::class, ['page_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBtns()
    {
        return $this->hasMany(Btn::class, ['id' => 'btn_id'])->viaTable(PageBtn::tableName(), ['page_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPers()
    {
        return $this->hasMany(Pers::class, ['page_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(Page::class, ['id' => 'parent_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPages()
    {
        return $this->hasMany(Page::class, ['parent_id' => 'id']);
    }

}
