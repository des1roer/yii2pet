<?php

namespace app\models;

/**
 * This is the model class for table "{{%btn}}".
 *
 * @property integer   $id
 * @property string    $link
 * @property integer   $sort
 * @property string    $img
 *
 * @property PageBtn[] $pageBtns
 * @property Page[]    $pages
 */
class Btn extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%btn}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sort'], 'integer'],
            [['link', 'img'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'link' => 'Link',
            'sort' => 'Sort',
            'img' => 'Img',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPageBtns()
    {
        return $this->hasMany(PageBtn::class, ['btn_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPages()
    {
        return $this->hasMany(Page::class, ['id' => 'page_id'])->viaTable('{{%page_btn}}', ['btn_id' => 'id']);
    }
}
