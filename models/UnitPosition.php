<?php

namespace app\models;

/**
 * This is the model class for table "{{%unit_position}}".
 *
 * @property integer  $id
 * @property integer  $unit_id
 * @property integer  $position_id
 *
 * @property Unit     $unit
 * @property Position $position
 */
class UnitPosition extends \yii\db\ActiveRecord
{
    public $position_alias;

    public static function find()
    {
        return parent::find()->with('position');
    }

    public function afterFind()
    {
        parent::afterFind();

        $this->position_alias = $this->position->alias;
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%unit_position}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['unit_id', 'position_id', 'pers_id'], 'required'],
            [['unit_id', 'position_id', 'pers_id'], 'integer'],
            [['unit_id', 'position_id'], 'unique', 'targetAttribute' => ['unit_id', 'position_id'], 'message' => 'The combination of Unit ID and Position ID has already been taken.'],
            [['unit_id'], 'exist', 'skipOnError' => true, 'targetClass' => Unit::class, 'targetAttribute' => ['unit_id' => 'id']],
            [['position_id'], 'exist', 'skipOnError' => true, 'targetClass' => Position::class, 'targetAttribute' => ['position_id' => 'id']],
            [
                ['pers_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Pers::class,
                'targetAttribute' => ['pers_id' => 'id']
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'pers_id' => 'Pers ID',
            'unit_id' => 'Unit ID',
            'position_id' => 'Position ID',
        ];
    }

    public function fields()
    {
        return [
            'id',
            'pers_id',
            'unit_id',
            'position_alias',
        ];
    }

    public function extraFields()
    {
        return [
            'pers',
            'unit',
            'position',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPers()
    {
        return $this->hasOne(Pers::class, ['id' => 'pers_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUnit()
    {
        return $this->hasOne(Unit::class, ['id' => 'unit_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPosition()
    {
        return $this->hasOne(Position::class, ['id' => 'position_id']);
    }
}
