<?php

namespace app\models;

use yii\helpers\Html;

/**
 * This is the model class for table "{{%pers}}".
 *
 * @property integer    $id
 * @property string     $name
 * @property integer    $lvl
 * @property integer    $money
 * @property integer    $race_id
 *
 * @property Race       $race
 * @property UserPers[] $userPers
 */
class Pers extends \yii\db\ActiveRecord
{
    const ITEM_LIST = 'itemList';
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%pers}}';
    }

    public static function getRaces()
    {
        return Race::findAll(['is_active' => 1]);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'race_id'], 'required'],
            [['name'], 'unique'],
            [['lvl', 'money', 'race_id'], 'integer'],
            [['lvl'], 'default', 'value' => 1],
            [['money'], 'default', 'value' => 0],
            [['name'], 'string', 'max' => 255],
            [
                ['race_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Race::class,
                'targetAttribute' => ['race_id' => 'id']
            ],
            [['unit_list', self::ITEM_LIST], 'safe'],
            [
                ['page_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Page::class,
                'targetAttribute' => ['page_id' => 'id']
            ],
            [
                ['map_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Page::class,
                'targetAttribute' => ['map_id' => 'id']
            ],
        ];
    }

    public function behaviors()
    {
        return [
            [
                'class' => \voskobovich\behaviors\ManyToManyBehavior::class,
                'relations' => [
                    'unit_list' => 'units',
                    self::ITEM_LIST => 'items',
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'lvl' => 'Lvl',
            'money' => 'Money',
            'race_id' => 'Раса',
        ];
    }

    public function extraFields()
    {
        return [
            'units' => function () {
                return $this->persUnits;
            },
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRace()
    {
        return $this->hasOne(Race::class, ['id' => 'race_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserPers()
    {
        return $this->hasMany(UserPers::class, ['pers_id' => 'id']);
    }

    public function getPersUnits()
    {
        return $this->hasMany(UnitPosition::class, ['pers_id' => 'id'])->indexBy('position_id');
    }

    public function getPersList()
    {
        return $this->hasMany(PersUnit::class, ['pers_id' => 'id']);
    }

    public function getUnits()
    {
        return $this->hasMany(Unit::class, ['id' => 'unit_id'])->viaTable(UnitPosition::tableName(), ['pers_id' => 'id']);
    }

    public function getUnitNames()
    {
        $data = $this->persUnits;
        $result = [];
        foreach ($data as $row) {
            $row = $row->unit;
            $result[] = Html::a($row->name, ['/unit/view', 'id' => $row->id], ['class' => 'btn btn-link']);
        }

        return ($result) ? implode($result) : '';
    }

    public function getUsers()
    {
        return $this->hasMany(User::class, ['id' => 'user_id'])->viaTable('{{%user_pers}}', ['pers_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPage()
    {
        return $this->hasOne(Page::class, ['id' => 'page_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMap()
    {
        return $this->hasOne(Page::class, ['id' => 'map_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItems()
    {
        return $this->hasMany(User::class, ['id' => 'user_id'])->viaTable('{{%user_pers}}', ['pers_id' => 'id']);
    }
}
