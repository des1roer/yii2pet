<?php

namespace app\models;

use amnah\yii2\user\models\Profile;
use amnah\yii2\user\models\UserAuth;
use amnah\yii2\user\models\UserToken;
use yii\helpers\ArrayHelper;
use yii\web\HttpException;

/**
 * This is the model class for table "{{%user}}".
 *
 * @property integer $id
 * @property integer $role_id
 * @property integer $status
 * @property string  $email
 * @property string  $username
 * @property string  $password
 * @property string  $auth_key
 * @property string  $access_token
 * @property string  $logged_in_ip
 * @property string  $logged_in_at
 * @property string  $created_ip
 * @property string      $created_at
 * @property string      $updated_at
 * @property string      $banned_at
 * @property string      $banned_reason
 *
 * @property Profile[]   $profiles
 * @property Role        $role
 * @property UserAuth[]  $userAuths
 * @property UserPers[]  $userPers
 * @property UserToken[] $userTokens
 */
class User extends \amnah\yii2\user\models\User
{
    const SCENARIO_PERS = 'pers';
    public
        $pers_id,
        $race_id,
        $pers_name;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [['role_id', 'status'], 'required'],
            [['role_id', 'status', 'race_id', 'pers_id', 'money'], 'integer'],
            [['logged_in_at', 'created_at', 'updated_at', 'banned_at'], 'safe'],
            [
                [
                    'email',
                    'username',
                    'password',
                    'auth_key',
                    'access_token',
                    'logged_in_ip',
                    'created_ip',
                    'banned_reason'
                ],
                'string',
                'max' => 255
            ],
            [['email'], 'unique'],
            [['username'], 'unique'],
            [
                ['role_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Role::class,
                'targetAttribute' => ['role_id' => 'id']
            ],
            ['pers_name', 'safe'],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'role_id' => 'Role ID',
            'status' => 'Status',
            'email' => 'Email',
            'username' => 'Username',
            'password' => 'Password',
            'auth_key' => 'Auth Key',
            'access_token' => 'Access Token',
            'logged_in_ip' => 'Logged In Ip',
            'logged_in_at' => 'Logged In At',
            'created_ip' => 'Created Ip',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'banned_at' => 'Banned At',
            'banned_reason' => 'Banned Reason',
        ];
    }

    /**
     * @param bool  $insert
     * @param array $changedAttributes
     * @throws HttpException
     * @throws \yii\db\Exception
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        $model = $this->getPers()->one();
        $new = false;
        if (!$model) {
            $model = new Pers();
            $new = true;
        }
        $model->setAttributes([
            'name' => $this->pers_name,
            'race_id' => $this->race_id,
        ]);

        if (!$model->save() && $this->scenario == self::SCENARIO_PERS) {
            throw new HttpException(422, implode(' ', $model->getFirstErrors()));
        }

        if ($new) {
            /** @noinspection MissedFieldInspection */
            \Yii::$app->db->createCommand()
                ->insert(UserPers::tableName(), [
                    'user_id' => $this->id,
                    'pers_id' => $model->id,
                ])->execute();
        }
    }


    public function getPers()
    {
        return $this->hasOne(Pers::class, ['id' => 'pers_id'])->viaTable(UserPers::tableName(), ['user_id' => 'id']);
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_PERS] = ['pers_id', 'race_id', 'pers_name'];
        return $scenarios;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfiles()
    {
        return $this->hasMany(Profile::class, ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRole()
    {
        return $this->hasOne(Role::class, ['id' => 'role_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserAuths()
    {
        return $this->hasMany(UserAuth::class, ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserPers()
    {
        return $this->hasMany(UserPers::class, ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserTokens()
    {
        return $this->hasMany(UserToken::class, ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPersItems()
    {
        return $this
            ->hasMany(PersItem::class, ['pers_id' => 'id'])
            ->andWhere(['>', 'count', 0])
            ->indexBy('item_id');
    }

    public function getItems()
    {
        return $this->hasMany(Item::class, ['id' => 'item_id'])
            ->viaTable(PersItem::tableName(), ['pers_id' => 'id'])
            ->indexBy('id');
    }
}
