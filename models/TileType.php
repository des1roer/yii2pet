<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%tile_type}}".
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property string $can_go
 * @property string $is_specific
 *
 * @property Map[] $maps
 */
class TileType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tile_type}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name', 'description'], 'string'],
            [['can_go', 'is_specific'], 'integer'],
            [['name'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'description' => 'Description',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMaps()
    {
        return $this->hasMany(Map::class, ['tile_type_id' => 'id']);
    }
}
