<?php

namespace app\models;

/**
 * This is the model class for table "{{%page_btn}}".
 *
 * @property integer $id
 * @property integer $page_id
 * @property integer $btn_id
 *
 * @property Btn     $btn
 * @property Page    $page
 */
class PageBtn extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%page_btn}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['page_id', 'btn_id'], 'required'],
            [['page_id', 'btn_id'], 'integer'],
            [['page_id', 'btn_id'], 'unique', 'targetAttribute' => ['page_id', 'btn_id'],
                'message' => 'The combination of Page ID and Btn ID has already been taken.'],
            [['btn_id'], 'exist', 'skipOnError' => true, 'targetClass' => Btn::class, 'targetAttribute' => ['btn_id' => 'id']],
            [['page_id'], 'exist', 'skipOnError' => true, 'targetClass' => Page::class, 'targetAttribute' => ['page_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'page_id' => 'Page ID',
            'btn_id' => 'Btn ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBtn()
    {
        return $this->hasOne(Btn::class, ['id' => 'btn_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPage()
    {
        return $this->hasOne(Page::class, ['id' => 'page_id']);
    }
}
