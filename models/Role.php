<?php

namespace app\models;

/**
 * This is the model class for table "{{%role}}".
 *
 * @property integer $id
 * @property string  $name
 * @property string  $created_at
 * @property string  $updated_at
 * @property integer $can_admin
 *
 * @property User[]  $users
 */
class Role extends \amnah\yii2\user\models\Role
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%role}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'unique'],
            [['created_at', 'updated_at'], 'safe'],
            [['can_admin'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'can_admin' => 'Can Admin',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::class, ['role_id' => 'id']);
    }
}
