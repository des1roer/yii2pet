<?php

use yii\helpers\VarDumper;

/**
 * Debug function
 * d($var);
 *
 * @param      $var
 * @param null $caller
 * @param int  $limit
 */
function d($var, $caller = null, int $limit = 10, bool $highlight = true)
{
    if (!isset($caller)) {
        $tmp_var = debug_backtrace(1);
        $caller = array_shift($tmp_var);
    }
    header('Content-Type: text/html; charset=utf-8');
    echo '<code>File: ' . $caller['file'] . ' / Line: ' . $caller['line'] . '</code>';
    echo '<pre>';
    VarDumper::dump($var, $limit, $highlight);
    echo '</pre>';
}

/**
 * Debug function with die() after
 * dd($var);
 *
 * @param     $var
 * @param int $limit
 */
function dc($var = null, int $limit = 5, bool $highlight = true)
{
    if ($var) {
        $tmp_var = debug_backtrace(1);
        $caller = array_shift($tmp_var);
        d($var, $caller, $limit, $highlight);
    }
    die();
}