<?php

/* @var $this \yii\web\View */

/* @var $content string */

use app\assets\AppAsset;
use app\widgets\Alert;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="http://faviconka.ru/ico/faviconka_ru_1489.ico" type="image/x-icon"/>
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    $user = \Yii::$app->user;
    $isAdmin = $user->can('admin');

    NavBar::begin([
        'brandLabel' => Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    /** @noinspection PhpUnhandledExceptionInspection */
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            ['label' => 'Start', 'url' => ['/pers'], 'visible' => !Yii::$app->user->isGuest],
            ['label' => 'Map', 'url' => ['/map'], 'visible' => !Yii::$app->user->isGuest],
            ['label' => 'Shop', 'url' => ['/shop'], 'visible' => !Yii::$app->user->isGuest],
            ['label' => 'Admin', 'items' => [
                ['label' => 'API', 'url' => ['/user']],
                ['label' => 'Аккаунты', 'url' => ['/user/admin']],
                ['label' => 'gii', 'url' => ['/gii']],
                ['label' => 'migrateUp', 'url' => ['/main']],
//                ['label' => 'migrateFresh', 'url' => ['/main/fresh']],
                ['label' => 'upload', 'url' => 'http://j96177ni.beget.tech/upload/', 'template' => '<a href="{url}" target="_blank">{label}</a>',],
                ['label' => 'pers', 'url' => ['/pers/admin']],
                ['label' => 'race', 'url' => ['/race']],
                ['label' => 'unit', 'url' => ['/unit']],
                ['label' => 'transport', 'url' => ['/transport']],
                ['label' => 'position', 'url' => ['/position']],
                ['label' => 'arena', 'url' => ['/site/arena']],
                ['label' => 'map', 'url' => ['/map/admin']],
                ['label' => 'db', 'url' => 'https://free1.beget.com/phpMyAdmin/admin.php?token=fdd1dc7b4a5d6decfa724a7cab46b908'],
                ['label' => 'git', 'url' => 'https://gitlab.com/des1roer/yii2pet/', 'template' => '<a href="{url}" target="_blank">{label}</a>',],
            ], 'visible' => $isAdmin], // check if user is an admin
            Yii::$app->user->isGuest ?
                ['label' => 'Login', 'url' => ['/user/login']] : // or ['/user/login-email']
                ['label' => 'Logout (' . Yii::$app->user->displayName . ')',
                    'url' => ['/user/logout'],
                    'linkOptions' => ['data-method' => 'post']],
        ],
    ]);
    NavBar::end();
    ?>
    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<!--<footer class="footer">-->
<!--    <div class="container">-->
<!--        <p class="pull-left">&copy; My Company --><? //= date('Y') ?><!--</p>-->
<!---->
<!--        <p class="pull-right">--><? //= Yii::powered() ?><!--</p>-->
<!--    </div>-->
<!--</footer>-->

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
