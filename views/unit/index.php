<?php

use app\models\Race;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Units';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="unit-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Unit', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php Pjax::begin(); ?>
    <?= /** @noinspection PhpUnhandledExceptionInspection */
    /** @noinspection PhpUndefinedVariableInspection */
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute' => 'name',
                'filter' => '',
            ],
            [
                'attribute' => 'hp',
                'filter' => '',
            ],
            [
                'attribute' => 'atk',
                'filter' => '',
            ],
            [
                'attribute' => 'img',
                'format' => 'html',
                'value' => function ($data) {
                    return Html::img($data->img, ['width' => '100']);
                },
            ],
            [
                'attribute' => 'race_id',
                'format' => 'raw',
                'label' => 'раса',
                'filter' => ArrayHelper::map(Race::find()->all(), 'id', 'name'),
                'value' => 'race.name'
            ],
            [
                'attribute' => 'parent_id',
                'format' => 'raw',
                'label' => 'Предок',
                'filter' => ArrayHelper::map(\app\models\Unit::find()->all(), 'id', 'name'),
                'value' => 'parent.name'
            ],
            [
                'format' => 'raw',
                'label' => 'Потомок',
                'value' => function ($data) {
                    return $data->child ? $data->child->name : null;
                },
            ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>

</div>
