<?php

use app\assets\VueAsset;
use yii\helpers\Html;

VueAsset::register($this);
$webPath = Yii::getAlias('@web');

Yii::$app->view->registerJs('var data = ' . json_encode(\yii\helpers\ArrayHelper::toArray($data)),
    \yii\web\View::POS_HEAD);
Yii::$app->view->registerJs('var user_id = ' . Yii::$app->user->id, \yii\web\View::POS_HEAD);
Yii::$app->view->registerJs('var unit = ' . json_encode(\yii\helpers\ArrayHelper::toArray($model)),
    \yii\web\View::POS_HEAD);

$this->registerJsFile(
    $webPath . '/scripts/functions.js', ['depends' => 'app\assets\AppAsset']
);
\app\assets\UnderscoreAsset::register($this);

/* @var $this yii\web\View */
/* @var $model app\models\Unit */

$this->title = $model->name;
$webPath = Yii::getAlias('@web');

$this->registerCssFile(
    $webPath . '/css/custom-photobox.css', ['depends' => 'app\assets\AppAsset']
);
$this->registerCssFile(
    "https://cdn.rawgit.com/yairEO/photobox/master/photobox/photobox.css", ['depends' => 'app\assets\AppAsset']
);

$this->registerJsFile(
    'https://cdn.rawgit.com/yairEO/photobox/master/photobox/jquery.photobox.js', ['depends' => 'app\assets\AppAsset']
);

$this->registerJsFile(
    $webPath . '/scripts/custom-photobox.js', ['depends' => 'app\assets\AppAsset']
);
?>
<div class="unit-view" id="app" align="center">
    <?= Html::img($model->img, ['width' => '200']); ?>
    <br>
    <br>
    Доступный транспорт
    <ul id='gallery'>
        <li v-for="row in rows" class="loaded"><a :href="row.img">
                <img :src="row.img" :title="row.name" @click="change"></a>
        </li>
    </ul>
    <aside class="sidebar-first" @click.prevent="prev">
        <a class="carousel-control left" href="#">
            <span class="glyphicon glyphicon-chevron-left"></span>
        </a>
    </aside>
    <aside class="sidebar-second" @click.prevent="next">
        <a class="carousel-control right" href="#">
            <span class="glyphicon glyphicon-chevron-right"></span>
        </a>
    </aside>
</div>
