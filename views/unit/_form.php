<?php

use app\models\Race;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Unit */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="unit-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'hp')->textInput() ?>

    <?= $form->field($model, 'atk')->textInput() ?>

    <?= $form->field($model, 'img')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'race_id')
        ->dropDownList(ArrayHelper::map(
            Race::find()->all(), 'id', 'name'),
            ['prompt' => 'Select...']
        ) ?>

    <?= $form->field($model, 'parent_id')
        ->dropDownList(ArrayHelper::map(
            \app\models\Unit::find()
                ->andWhere(['!=', 'id', $model->id])
                ->andWhere([
                    'NOT IN',
                    'id',
                    \app\models\Unit::find()->select('parent_id')->andWhere(['NOT', ['parent_id' => null]])
                ])
                ->all(), 'id', 'name'),
            ['prompt' => 'Select...']
        ) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update',
            ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
