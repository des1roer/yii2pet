<?php

app\assets\VueAsset::register($this);
$webPath = Yii::getAlias('@web');

$this->registerCssFile(
    'http://cdn.leafletjs.com/leaflet-0.7/leaflet.css', ['depends' => 'app\assets\AppAsset']
);
$this->registerJsFile(
    'http://cdn.leafletjs.com/leaflet-0.7/leaflet.js', ['depends' => 'app\assets\AppAsset']
);
$this->registerJsFile(
    $webPath . '/scripts/map.js', ['depends' => 'app\assets\AppAsset']
);
$this->registerCssFile(
    $webPath . '/css/site.css', ['depends' => 'app\assets\AppAsset']
);
$this->registerCssFile(
    $webPath . '/css/style.css', ['depends' => 'app\assets\AppAsset']
);
?>

<script type="text/x-template" id="modal-template">
    <transition name="modal">
        <div class="modal-mask">
            <div class="modal-wrapper" @click="write">
                <div class="modal-container" id="modal-container">

                    <div class="modal-header">
                        <slot name="header">
                            <h3>SIGN IN</h3>
                        </slot>
                    </div>

                    <div class="modal-body">
                        <slot name="body">
                        </slot>
                    </div>

                    <div class="modal-footer">
                        <slot name="footer">
                        </slot>
                    </div>
                </div>
            </div>
        </div>
    </transition>
</script>

<div class="container" id="app" style="margin-top: 10px; width: 99%">
    <!--    <div v-if="false" class="modal-mask">-->
    <!--        <div class="modal-wrapper">-->
    <!--            <center>-->
    <!--                <div class="loader"></div>-->
    <!--            </center>-->
    <!--        </div>-->
    <!--    </div>-->


    <modal v-show="showModal">
        <div slot="body">
            {{ msg }}
        </div>
        <div slot="footer">
            <!--            <button class="btn btn-success" @click="pass()">-->
            <!--                Sign In-->
            <!--            </button>-->
        </div>
    </modal>
</div>
<div style="height:500px" id="map"></div>
