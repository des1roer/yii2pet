<?php

/** @link https://www.tablesgenerator.com/html_tables */

use app\assets\VueAsset;

VueAsset::register($this);
$webPath = Yii::getAlias('@web');

Yii::$app->view->registerJs('var data = ' . json_encode(\yii\helpers\ArrayHelper::toArray($data)), \yii\web\View::POS_HEAD);
Yii::$app->view->registerJs('var user = ' . Yii::$app->user->id, \yii\web\View::POS_HEAD);
Yii::$app->view->registerJs('var webPath = ' . json_encode($webPath), \yii\web\View::POS_HEAD);

$this->registerJsFile(
    $webPath . '/scripts/functions.js', ['depends' => 'app\assets\AppAsset']
);

$this->registerJsFile(
    $webPath . '/scripts/arena.js', ['depends' => 'app\assets\AppAsset']
);

$this->registerCssFile(
    $webPath . '/css/arena.css', ['depends' => 'app\assets\AppAsset']
);
\app\assets\UnderscoreAsset::register($this);

?>
<div class="container" id="app" style="margin-top: 10px; width: 99%">
    <table class="table table-bordered">
        <tr>
            <td colspan="2">
                <div align="center">
                    <img :src="positions.my.big" height="200px">
                </div>
            </td>
            <td>
                <div align="center">
                    <a href="#" class="btn btn-success" role="button" @click="start" v-show="show">Start</a>
                    <button class="btn btn-success" onclick="location.reload()">
                        Reload
                    </button>
                </div>
            </td>
            <td colspan="2">
                <div align="center">
                    <img :src="positions.enemy.big" height="200px">
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div align="center">
                    <img :src="positions.my.lastFar" height="200px">
                </div>
            </td>
            <td>
                <div align="center">
                    <img :src="positions.my.firstFar" height="200px">
                </div>
            </td>
            <td rowspan="2">
                <div align="center">
                    <p v-html="loger"></p>

                </div>
            </td>
            <td>
                <div align="center">
                    <img :src="positions.enemy.firstFar" height="200px">
                </div>
            </td>
            <td>
                <div align="center">
                    <img :src="positions.enemy.lastFar" height="200px">
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div align="center">
                    <img :src="positions.my.lastClose" height="200px">
                </div>
            </td>
            <td>
                <div align="center">
                    <img :src="positions.my.firstClose" height="200px">
                </div>
            </td>
            <td>
                <div align="center">
                    <img :src="positions.enemy.firstClose" height="200px">
                </div>
            </td>
            <td>
                <div align="center">
                    <img :src="positions.enemy.lastClose" height="200px">
                </div>
            </td>
        </tr>
    </table>
</div>