<?php

/** @link https://www.tablesgenerator.com/html_tables */
/** @see \app\controllers\ShopController::actionIndex */

/** @see web/scripts/shop.js */

use app\assets\VueAsset;

VueAsset::register($this);
$webPath = Yii::getAlias('@web');

Yii::$app->view->registerJs('var shop = ' . json_encode(\yii\helpers\ArrayHelper::toArray($shop)),
    \yii\web\View::POS_HEAD);
Yii::$app->view->registerJs('var user = ' . json_encode(\yii\helpers\ArrayHelper::toArray($user)),
    \yii\web\View::POS_HEAD);
Yii::$app->view->registerJs('var webPath = ' . json_encode($webPath), \yii\web\View::POS_HEAD);

$this->registerJsFile(
    $webPath . '/scripts/functions.js', ['depends' => 'app\assets\AppAsset']
);

$this->registerJsFile(
    $webPath . '/scripts/shop.js', ['depends' => 'app\assets\AppAsset']
);

$this->registerCssFile(
    $webPath . '/css/arena.css', ['depends' => 'app\assets\AppAsset']
);
$this->registerCssFile(
    $webPath . '/css/style.css', ['depends' => 'app\assets\AppAsset']
);
\app\assets\UnderscoreAsset::register($this);

?>

<script type="text/x-template" id="modal-template">
    <transition name="modal">
        <div class="modal-mask">
            <div class="modal-wrapper" @click="write">
                <div class="modal-container" id="modal-container">

                    <div class="modal-header">
                        <slot name="header">
                        </slot>
                    </div>

                    <div class="modal-body">
                        <slot name="body">
                        </slot>
                    </div>

                    <div class="modal-footer">
                        <slot name="footer">
                        </slot>
                    </div>
                </div>
            </div>
        </div>
    </transition>
</script>

<div class="container" id="app" style="margin-top: 10px; width: 99%">
    <modal v-show="showModal">
        <div slot="header">
            {{ user.pers.money }}
        </div>
        <div slot="body">
            {{ msg }}
        </div>
        <div slot="footer">
            <button class="btn btn-success" @click="buy()" v-show="isBuy">
                Buy
            </button>
            <button class="btn btn-success" @click="sale()" v-show="isSale">
                Sale
            </button>
        </div>
    </modal>

    <table class="table table-bordered">
        <tr>
            <th>Персонаж</th>
            <th></th>
            <th>Торговец</th>
        </tr>
        <tr>
            <td>
                <div v-for="userItem in userItems" @click="saleItem(userItem)">
                    {{ userItem['item'].name }} {{ userItem.count }}
                </div>
            </td>
            <td></td>
            <td>
                <div v-for="shopItem in shopItems" @click="buyItem(shopItem['item'])">
                    {{ shopItem['item'].name }} {{ shopItem['item'].cost }} {{ shopItem.count }}
                </div>
            </td>
        <tr>
            <td></td>
            <td></td>
            <td></td>
        </tr>
    </table>
</div>