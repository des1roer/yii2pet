<?php

use app\assets\VueAsset;

VueAsset::register($this);
$webPath = Yii::getAlias('@web');

Yii::$app->view->registerJs('var data = ' . json_encode(\yii\helpers\ArrayHelper::toArray($data)), \yii\web\View::POS_HEAD);
Yii::$app->view->registerJs('var user_id = ' . Yii::$app->user->id, \yii\web\View::POS_HEAD);

$this->registerJsFile(
    $webPath . '/scripts/functions.js', ['depends' => 'app\assets\AppAsset']
);

$this->registerJsFile(
    $webPath . '/scripts/army.js', ['depends' => 'app\assets\AppAsset']
);

$this->registerCssFile(
    $webPath . '/css/army.css', ['depends' => 'app\assets\AppAsset']
);
\app\assets\UnderscoreAsset::register($this);
?>

<body>
<div id="app" class="parent-wrapper">
    <div class="parent">
        <img v-for="unit in units" class="child" :src="unit.img" :alt="unit.name" width="0" @click="change(unit)">
    </div>
</div>
</body>