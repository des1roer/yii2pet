<?php

use yii\helpers\Html;
use yii\helpers\Url;

app\assets\VueAsset::register($this);
$webPath = Yii::getAlias('@web');

$this->registerCssFile(
    'https://unpkg.com/leaflet@1.3.1/dist/leaflet.css', ['depends' => 'app\assets\AppAsset']
);
$this->registerJsFile(
    'https://unpkg.com/leaflet@1.3.1/dist/leaflet.js', ['depends' => 'app\assets\AppAsset']
);
$this->registerJsFile(
    $webPath . '/scripts/functions.js', ['depends' => 'app\assets\AppAsset']
);
$this->registerJsFile(
    $webPath . '/scripts/map.js', ['depends' => 'app\assets\AppAsset']
);
$this->registerCssFile(
    $webPath . '/css/site.css', ['depends' => 'app\assets\AppAsset']
);
$this->registerCssFile(
    $webPath . '/css/style.css', ['depends' => 'app\assets\AppAsset']
);

Yii::$app->view->registerJs(
        'var data = ' . json_encode(\yii\helpers\ArrayHelper::toArray($data)),
    \yii\web\View::POS_HEAD
);

Yii::$app->view->registerJs('var webPath = ' . json_encode($webPath), \yii\web\View::POS_HEAD);

Yii::$app->view->registerJs('var start = ' . $startPosition, \yii\web\View::POS_HEAD);
Yii::$app->view->registerJs('var minLevel = ' . $minLevel, \yii\web\View::POS_HEAD);
Yii::$app->view->registerJs('var maxLevel = ' . $maxLevel, \yii\web\View::POS_HEAD);
?>

<script type="text/x-template" id="modal-template">
    <transition name="modal">
        <div class="modal-mask">
            <div class="modal-wrapper" @click="write">
                <div class="modal-container" id="modal-container">

                    <div class="modal-header">
                        <slot name="header">
                            <h3>SIGN IN</h3>
                        </slot>
                    </div>

                    <div class="modal-body">
                        <slot name="body">
                        </slot>
                    </div>

                    <div class="modal-footer">
                        <slot name="footer">
                        </slot>
                    </div>
                </div>
            </div>
        </div>
    </transition>
</script>
<?= Html::a('Generate', ['/map/generate'], ['id' => 'el', 'class'=>'btn btn-primary']) ?>

<div class="container" id="app" style="margin-top: 10px; width: 99%">
    <!--    <div v-if="false" class="modal-mask">-->
    <!--        <div class="modal-wrapper">-->
    <!--            <center>-->
    <!--                <div class="loader"></div>-->
    <!--            </center>-->
    <!--        </div>-->
    <!--    </div>-->


    <modal v-show="showModal">
        <div slot="body">
            {{ msg }}
        </div>
        <div slot="footer">
            <a :href="url" v-show="is_specific" class="btn btn-success" >
                Action
            </a>
            <button class="btn btn-success" @click="edit()">
                Edit
            </button>
        </div>
    </modal>
</div>
<div style="height:500px" id="map"></div>
