<?php

/** @see \app\controllers\PersController::actionIndex */

use app\assets\VueAsset;

VueAsset::register($this);
$webPath = Yii::getAlias('@web');
Yii::$app->view->registerJs('var data = ' . json_encode(\yii\helpers\ArrayHelper::toArray($data)), \yii\web\View::POS_HEAD);
Yii::$app->view->registerJs('var user_id = ' . Yii::$app->user->id, \yii\web\View::POS_HEAD);

$this->registerJsFile(
    $webPath . '/scripts/start.js', ['depends' => 'app\assets\AppAsset']
);

$this->registerCssFile(
    $webPath . '/css/style.css', ['depends' => 'app\assets\AppAsset']
);
\app\assets\UnderscoreAsset::register($this);
?>

<div id="app">
    <section class="content">
        <div class="columns">
            <main class="main">
                <div class="inner" align='center'>{{val}}<br>
                    <div class="form-group" :class="{'has-error' : hasError}">
                        <input class="form-control" v-model="name" style="width: 200px;  margin: 0 auto;">
                    </div>
                    <br>
                    <img :src="url" height="500">
                    <br><br>
                    <a href="unit/army/" class="btn btn-success" role="button" v-show='!hasError' @click.prevent="army">Ok</a>
                </div>
            </main>
            <aside class="sidebar-first" @click.prevent="prev">
                <a class="carousel-control left" href="#">
                    <span class="glyphicon glyphicon-chevron-left"></span>
                </a>
            </aside>
            <aside class="sidebar-second" @click.prevent="next">
                <a class="carousel-control right" href="#">
                    <span class="glyphicon glyphicon-chevron-right"></span>
                </a>
            </aside>
        </div>
    </section>
</div>
