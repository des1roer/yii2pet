<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TileType */

$this->title = 'Update Tile Type: {nameAttribute}';
$this->params['breadcrumbs'][] = ['label' => 'Tile Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tile-type-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
