<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\TileType */

$this->title = 'Create Tile Type';
$this->params['breadcrumbs'][] = ['label' => 'Tile Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tile-type-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
