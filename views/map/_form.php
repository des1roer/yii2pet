<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
$webPath = Yii::getAlias('@web');
/* @var $this yii\web\View */
/* @var $model app\models\Map */
/* @var $form yii\widgets\ActiveForm */
/* @see \app\controllers\MapController */
?>

<div class="map-form">

    <?php $form = ActiveForm::begin(); ?>

    Сообщение
    <?= Html::input('text', "Map[coords][properties][msg]", $model->coords['properties']['msg'], ['class' => 'form-control']) ?>

    <?= $form->field($model, 'title')->textarea(['rows' => 6]) ?>

    <?php
    $data = \app\models\TileType::find()->all();
    echo $form->field($model, 'tile_type_id')->dropDownList(
        ArrayHelper::map($data, 'id', 'name')
    ) ?>

    <?= $form->field($model, 'model')->dropDownList(
        [
            '',
            'shop' => 'shop',
            'pers' => 'pers',
        ],
        [
            'id' => 'model',
            'onchange' => "

                                   $.ajax({
                                     url: \"" . $webPath ."/map/subcat\",
                                     type: \"post\",
                                     data: { model:  $(this).val() },
                                     success: function(data) {
                                           var opts = '<option value=\"\">-- Select --</option>';
                                                data = $.parseJSON(data)
                                             for (var i in data) {
                                    
                                                 opts += '<option value=\"' + i + '\">' + data[i] + '</option>';
                                    
                                             }

                                            document.getElementById('model_id').innerHTML = opts;

                                        }

                                    });"
        ]
    ) ?>
    <?php
    if (!$model->isNewRecord && isset($model->model))
    {
        $subcatList = ('app\\models\\'.ucfirst ($model->model))::find()->all();
        $subcatList_ = ArrayHelper::map($subcatList, 'id', 'name');
    }
    else
        $subcatList_ = [];
    ?>
    <?= $form->field($model, 'model_id')->dropDownList($subcatList_, ['value' => !(empty($model->model_id)) ? $model->model_id : '', 'id' => 'model_id']) ?>
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
