<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PersItem */

$this->title = 'Update Pers Item: {nameAttribute}';
$this->params['breadcrumbs'][] = ['label' => 'Pers Items', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="pers-item-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
