<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\PersItem */

$this->title = 'Create Pers Item';
$this->params['breadcrumbs'][] = ['label' => 'Pers Items', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pers-item-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
