<?php

use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Pers';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pers-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Pers', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php Pjax::begin(['enablePushState' => false]) ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'name',
            'lvl',
            'money',
            [
                'attribute' => 'race_id',
                'format' => 'raw',
                'label' => 'раса',
                'filter' => $races,
                'value' => 'race.name'
            ],
            [
                'label' => 'Изображение',
                'format' => 'html',
                'value' => function ($data) {
                    return Html::img($data->race->img, ['width' => '100']);
                },
            ],
            [
                'format' => 'raw',
                'label' => 'Армия',
                'value' => function ($data) {
                    /** @see \app\models\Pers::getUnitNames */
                    return $data->unitNames;
                },
            ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
