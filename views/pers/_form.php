<?php

use app\models\{
    Page, Race
};
use yii\helpers\{
    ArrayHelper, Html
};
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Pers */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pers-form" id="persform">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'lvl')->textInput() ?>

    <?= $form->field($model, 'money')->textInput() ?>

    <?= $form->field($model, 'race_id')->dropDownList(ArrayHelper::map(Race::find()->all(), 'id', 'name'),
        ['prompt' => 'Select...']) ?>
    <?= $form->field($model, 'page_id')->dropDownList(ArrayHelper::map(Page::find()->all(), 'id', 'name'),
        ['prompt' => 'Select...']) ?>
    <?php foreach ($positions as $pos) {
        $val = null;
        if (!empty($persUnits[$pos->id])) {
            $val = $persUnits[$pos->id]->unit_id;
        } ?>
        <label class="control-label" for="username"><?= $pos->name ?></label>
        <?= /** @noinspection PhpUndefinedVariableInspection */
        Html::dropDownList("pos[" . $pos->id . "]", $val, $units,
            ['prompt' => 'Select...', 'class' => 'form-control']) ?>
    <?php } ?>
    <br/>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update',
            ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>

</div>
