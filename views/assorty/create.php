<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Assorty */

$this->title = 'Create Assorty';
$this->params['breadcrumbs'][] = ['label' => 'Assorties', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="assorty-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
