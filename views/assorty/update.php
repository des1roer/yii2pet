<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Assorty */

$this->title = 'Update Assorty: {nameAttribute}';
$this->params['breadcrumbs'][] = ['label' => 'Assorties', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="assorty-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
